package search;

import static org.junit.Assert.*;
import junit.framework.Assert;


import map.Map;
import factories.MapFactory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ai.evaluation.experiment.ComplexEvaluation;
import ai.evaluation.Evaluation;
import ai.search.SearchStrategy;
import ai.search.SortingAlphaBeta;

public class SortingAlphaBetaTest {


    final int timeFail = 1000;
    final int timePass = 1500;
    final int time = 1500;
    Map map;
    Evaluation e;
    SearchStrategy mFinder;
    final int buffer = 200;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        map = MapFactory.createFromFile("./maps/map8_midgame.map");
        e = new ComplexEvaluation();
        mFinder = new SortingAlphaBeta(map, 1, e);
    }

    @Test
    public void testTime_ShouldFail(){
        long start = (long)(System.nanoTime()/1000);
        mFinder.getMove(100, timeFail-buffer);
        long end = (long)(System.nanoTime()/1000);
        Assert.assertTrue((time-1000) >= (start -end));
    }

    @Test
    public void testTime_ShouldPass(){
        long start = (long)(System.nanoTime()/1000);
        mFinder.getMove(100, timePass-buffer);
        long end = (long)(System.nanoTime()/1000);
        Assert.assertTrue(timePass <= (end-start));
    }
}
