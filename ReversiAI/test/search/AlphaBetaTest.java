package search;


import ai.search.AlphaBeta;
import junit.framework.Assert;
import ai.evaluation.*;
import ai.search.*;
import experiment.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ai.evaluation.experiment.ComplexEvaluation;
import factories.MapFactory;
import map.Map;


public class AlphaBetaTest {

    final int timeFail = 1000;
    final int timePass = 1500;
    final int time = 1500;
    Map map;
    Evaluation e;
    SearchStrategy mFinder;
    final int buffer = 200;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        map = MapFactory.createFromFile("./maps/map8_midgame.map");
        e = new ComplexEvaluation();
        mFinder = new AlphaBeta(map, 1, e);
    }

    @Test
    public void testTime_ShouldFail(){
        long start = System.nanoTime();
        mFinder.getMove(100, timeFail-buffer);
        long end = System.nanoTime();
        Assert.assertTrue(time >= (start -end));
    }

    @Test
    public void testTime_ShouldPass(){
        long start = (long)(System.nanoTime()/1000);
        mFinder.getMove(100, timePass-buffer);
        long end = (long)(System.nanoTime()/1000);
        Assert.assertTrue(timePass <= (end-start));
    }
}
