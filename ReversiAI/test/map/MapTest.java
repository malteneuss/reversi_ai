package map;

import factories.MapFactory;
import org.junit.Test;
import static org.junit.Assert.*;

public class MapTest {

    public MapTest() {
    }

    @Test
    public void testInversionAndReversion() {
        String mapAsString = ""
                + "8\n"//
                + "1\n"//
                + "1 1\n"//
                + "1 8\n"//
                + "1 2 3 4 5 6 7 8\n"//Map
                ;
   
        Map m = MapFactory.createFromString(mapAsString);

        Map copy = m.copy();
        copy.inverse();
        //2 3 4 5 6 7 8 1
        for (int i = 0; i < 7; i++) {
            assertTrue(copy.isPlayerAt(i + 2, i, 0));
        }
        //undo
        copy.reverse();
        for (int i = 0; i < 8; i++) {
            assertTrue(m.getChar(i, 0) == copy.getChar(i, 0));
        }


    }
   
}