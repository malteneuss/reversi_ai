package movegenerator;

import factories.MapFactory;
import factories.MoveFactory;
import factories.MoveGeneratorFactory;
import java.util.ArrayList;
import java.util.List;
import map.Map;
import move.Move;
import org.junit.Test;
import static org.junit.Assert.*;

public class MoveGeneratorTest {

    public MoveGeneratorTest() {
    }

    @Test
    public void testIfAllValidMovesAreGenerated() {
        String mapAsString = ""//
                + "8\n" //8 player
                + "1\n"//1 Override
                + "1 3\n"//1 Bomb
                + "5 5\n"//5 x 5 Map
                //
                + "1 1 0 1 6\n"//
                + "1 2 3 7 0\n"//
                + "x 4 5 - 8\n"//
                + "- x i b -\n"//
                + "0 c - x 0\n"//
                //Transition
                + "4 0 2 <-> 4 4 1\n"; //4 0 Right - 4 4 UP_Right
        Map map = MapFactory.createFromString(mapAsString);



        MoveGenerator moveGenerator = MoveGeneratorFactory.getFrom(map, 1);
        List<Move> moves = getAllMoves(moveGenerator);
        //assertTrue(moves.size() == 14);

        contains(moves, 2, 1);
        contains(moves, 3, 1);
        contains(moves, 4, 1);
        contains(moves, 0, 2);
        contains(moves, 1, 2);
        contains(moves, 2, 2);
        contains(moves, 1, 3);
        contains(moves, 2, 3);
        contains(moves, 3, 3);
        contains(moves, 1, 4);
        contains(moves, 3, 4);
        contains(moves, 4, 4);     
    }

    private List<Move> getAllMoves(MoveGenerator generator) {
        List<Move> moves = new ArrayList<>();
        while (generator.hasNext()) {
            moves.add(generator.next());
        }
        return moves;
    }

    private void contains(List<Move> moves, int x, int y) {
        boolean contains = false;
        for (Move m : moves) {
            if (m.getX() == x && m.getY() == y) {
                contains = true;
                break;
            }
        }
        assertTrue("Does not contain(" + x + ", " + y + ")", contains);
    }
}