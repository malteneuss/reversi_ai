//package experiment;
//
//import game.Direction;
//import java.awt.Point;
//import java.util.ArrayList;
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.junit.Ignore;
//
//public class MapTest {
//
//    private Map complexMap;
//
//    public MapTest() {
//    }
//
//    @BeforeClass
//    public static void setUpClass() {
//    }
//
//    @AfterClass
//    public static void tearDownClass() {
//    }
//
//    @Before
//    public void setUp() {
//        complexMap = createComplexTestMap();
//    }
//
//    @After
//    public void tearDown() {
//    }
//
//    private Map createComplexTestMap() {
//
//        String map = ""//
//                + "8\n" //8 player
//                + "1\n"//1 Override
//                + "1 3\n"//1 Bomb
//                + "5 5\n"//5 x 5 Map
//                //
//                + "1 1 0 1 6\n"//
//                + "1 2 3 7 0\n"//
//                + "x 4 5 - 8\n"//
//                + "- x i b -\n"//
//                + "0 c - x 0\n"//
//                //Transition
//                + "4 0 2 <-> 4 4 1\n"; //4 0 Right - 4 4 UP_Right
//        return MapFactory.createFromString(map);
//
//    }
//
//    ///Valid Moves//////////////////////////////


//
//    @Test
//    public void stoneOnEnclosingEmptyField_ShouldBeValid() {
//        // assertTrue(complexMap.isValidPlayerMoveAt(1, 4, 1));
//    }
//
//    @Test
//    public void stoneOnEnclosingBonusField_ShouldBeValid() {
//        assertTrue(complexMap.isBonusAt(3, 3));
//        // assertTrue(complexMap.isValidPlayerMoveAt(1, 3, 3));
//    }
//
//    @Test
//    public void stoneOnEnclosingInversionField_ShouldBeValid() {
//        //assertTrue(complexMap.isValidPlayerMoveAt(1, 2, 3));
//    }
//
//    @Test
//    public void stoneOnEnclosingChoiceField_ShouldBeValid() {
//        //assertTrue(complexMap.isValidPlayerMoveAt(1, 1, 4));
//    }
//
//    @Test
//    public void stoneOnEnclosingEmptyFieldOverTransition_ShouldBeValid() {
//        // assertTrue(complexMap.isValidPlayerMoveAt(1, 4, 4));
//    }
//
//    @Test
//    public void overrideStoneOnEnclosingExpansionField_ShouldBeValid() {
//        assertTrue(complexMap.getOverrideCountOf(1) > 0);
//        //assertTrue(complexMap.isValidPlayerMoveAt(1, 1, 3));
//    }
//
//    @Test
//    public void overrideStoneOnNotEnclosingExpansionField_ShouldBeValid() {
//        assertTrue(complexMap.getOverrideCountOf(1) > 0);
//        // assertTrue(complexMap.isValidPlayerMoveAt(1, 0, 2));
//    }
//
//    @Test
//    public void overrideStoneOnEnclosingEmptyFieldOverTransition_ShouldBeValid() {
//        assertTrue(complexMap.getOverrideCountOf(1) > 0);
//        // assertTrue(complexMap.isValidPlayerMoveAt(1, 4, 4));
//    }
//
//    //Invalid Moves///////////////////
//    @Test
//    public void stoneOnNotEnclosingNearbyEmptyField_ShouldBeInvalid() {
//        //assertFalse(complexMap.isValidPlayerMoveAt(1, 2, 0));
//    }
//
//    @Test
//    public void stoneOnNotEnclosingFarAwayEmptyField_ShouldBeInvalid() {
//        //assertFalse(complexMap.isValidPlayerMoveAt(1, 0, 4));
//    }
//
//    @Test
//    public void stoneOnHole_ShouldBeInvalid() {
//        // assertFalse(complexMap.isValidPlayerMoveAt(1, 0, 3));
//        //assertFalse(complexMap.isValidPlayerMoveAt(1, 4, 3));
//    }
//
//
//    @Test
//    public void stoneOnNonEmptyTilesWithNoOverrideStones_ShouldBeInvalid() {
//        String map = ""
//                + "8\n"//
//                + "0\n"//
//                + "0 0\n"//
//                + "1 6\n"//
//                + "1 2 3 8 x 0\n";//
//        Map lmap = MapFactory.createFromString(map);//mb.build();
//        assertEquals(0, lmap.getOverrideCountOf(1));
////        assertTrue(lmap.isValidPlayerMoveAt(1, 5, 0));
////        assertFalse(lmap.isValidPlayerMoveAt(1, 4, 0));
////        assertFalse(lmap.isValidPlayerMoveAt(1, 3, 0));
////        assertFalse(lmap.isValidPlayerMoveAt(1, 2, 0));
////        assertFalse(lmap.isValidPlayerMoveAt(1, 2, 0));
//    }

//
//    ///////
//    //Valid stone placements and results
//    @Test
//    public void testRecoloringAndBombOnStarMap_ShouldRecolorAndBombAll() {
//        String map = ""
//                + "8\n"//8 player
//                + "0\n"//
//                + "1 2\n"//
//                + "5 5\n"//
//                + "1 - 1 - 1\n"//
//                + "- 2 3 7 -\n"//
//                + "1 4 0 6 1\n"//
//                + "- x 8 5 -\n"//
//                + "1 - 1 - 1\n";//
//
//        Map m = MapFactory.createFromString(map);
//        assertTrue(m.getBombCountOf(1) > 0);
//        assertFalse(m.getOverrideCountOf(1) > 0);
//
//        for (int y = 0; y < m.getHeight(); y++) {
//            for (int x = 0; x < m.getWidth(); x++) {
//                if (x == 2 && y == 2) {//Empty in the middle
//                    //assertTrue(m.isValidPlayerMoveAt(1, x, y));
//                } else {
//                    //assertFalse(String.format("(%d,%d)\n", x, y), m.isValidPlayerMoveAt(1, x, y));
//                }
//            }
//        }
//        m.setAt(1, 2, 2);
//        //1 - 1 - 1
//        //- 1 1 1 -
//        //1 1 1 1 1
//        //- 1 1 1 -
//        //1 - 1 - 1
//        for (int y = 0; y < m.getHeight(); y++) {
//            for (int x = 0; x < m.getWidth(); x++) {
//                if (!m.isHoleAt(x, y)) {//Empty in the middle
//                    assertTrue(m.isPlayerXAt(1, x, y));
//                }
//            }
//        }
//        m.changeToPhase2();
//        m.setAt(1, 2, 2);//bomb everything to a Hole
//        assertEquals(0, m.getBombCountFor(1));
//        assertFalse(m.hasBomb(1));
//        for (int y = 0; y < m.getHeight(); y++) {
//            for (int x = 0; x < m.getWidth(); x++) {
//                assertTrue(m.isHoleAt(x, y));
//
//            }
//        }
//
//

//
//    @Test
//    public void checkExpansionRule() {
//        //x
//        MapBuilder mb = new MapBuilder();
//        mb.startNewWith(1, 1);
//        mb.setNumOfPlayers(8);
//        mb.setExpansionAt(0, 0);
//        mb.setNumOfOverrideStones(1);
//        Map m = mb.build();
//        for (int i = 1; i <= 8; i++) {
//            assertTrue(m.isValidPlayerMoveAt(i, 0, 0));
//            assertEquals(1, m.getOverrideCountFor(i));
//        }
//
//        m.setAt(1, 0, 0);
//        assertEquals(0, m.getOverrideCountFor(1));
//
//        assertFalse(m.isExpansionAt(0, 0));
//    }
//
//    @Test
//    public void transitionsTest() {
//        //1 2
//        //0 3
//        //
//        //Transitions
//        //0 0 Up_LEFT 1 0 UP
//        //1 1 DOWN 11 RIGHT
//        MapBuilder mb = new MapBuilder();
//        mb.startNewWith(2, 2);
//        mb.setPlayerAt(1, 0, 0);
//        mb.setPlayerAt(2, 1, 0);
//        mb.setEmptyAt(0, 1);
//        mb.setPlayerAt(3, 1, 1);
//        mb.addTransistion(0, 0, Direction.UP_LEFT, 1, 0, Direction.UP);
//        mb.addTransistion(1, 1, Direction.DOWN, 1, 1, Direction.RIGHT);
//        mb.setNumOfOverrideStones(0);
//
//        Map lmap = mb.build();
//        assertEquals(0, lmap.getOverrideCountFor(1));
//        assertTrue(lmap.isValidPlayerMoveAt(1, 0, 1));
//        lmap.setAt(1, 0, 1);
//        assertTrue(lmap.isPlayerXAt(1, 0, 0));
//        assertTrue(lmap.isPlayerXAt(1, 1, 0));
//        assertTrue(lmap.isPlayerXAt(1, 0, 1));
//        assertTrue(lmap.isPlayerXAt(1, 1, 1));
//
//
//
//
//    }
//
//    @Test
//    public void testInversion() {
//        //1 2 3 4 5 6 7 8 - 1 x i
//        MapBuilder mb = new MapBuilder();
//        mb.setNumOfPlayers(8);
//        mb.startNewWith(12, 1);
//        mb.setNumOfOverrideStones(0);
//        mb.setNumOfBombs(1);
//        mb.setBombStrength(12);
//        for (int i = 0; i < 8; i++) {
//            mb.setPlayerAt(i + 1, i, 0);
//        }
//        mb.setHoleAt(8, 0);
//        mb.setPlayerAt(1, 9, 0);
//        mb.setExpansionAt(10, 0);
//        mb.setInversionAt(11, 0);
//
//        Map m = mb.build();
//        assertTrue(m.isValidPlayerMoveAt(1, 11, 0));
//
//        m.setAt(1, 11, 0);
//        //2 3 4 5 6 7 8 1 - 2 2 2
//        for (int i = 0; i < 7; i++) {
//            assertTrue(m.isPlayerXAt(i + 2, i, 0));
//        }
//        assertTrue(m.isPlayerXAt(1, 7, 0));
//        assertTrue(m.isHoleAt(8, 0));
//        assertTrue(m.isPlayerXAt(2, 9, 0));
//        assertTrue(m.isPlayerXAt(2, 10, 0));
//        assertTrue(m.isPlayerXAt(2, 11, 0));
//
//        m.changeToPhase2();
//        m.setAt(1, 0, 0); //bomb left shouldn reach over Hole at (9,0)
//        //2 3 4 5 6 7 8 1 - 2 2 2
//        for (int i = 0; i < 9; i++) {
//            assertTrue(m.isHoleAt(i, 0));
//        }
//        assertTrue(m.isPlayerXAt(2, 9, 0));
//        assertTrue(m.isPlayerXAt(2, 10, 0));
//        assertTrue(m.isPlayerXAt(2, 11, 0));
//
//
//
//    }
//

//
//    @Test
//    public void testSpecialTiles() {
//        //1 2 3 4 5 6 7 8 x b b c
//        MapBuilder mb = new MapBuilder();
//        mb.startNewWith(12, 1);
//        mb.setNumOfPlayers(8);
//        mb.setNumOfBombs(0);
//        mb.setNumOfOverrideStones(0);
//        for (int i = 0; i < 8; i++) {
//            mb.setPlayerAt(i + 1, i, 0);
//        }
//        mb.setExpansionAt(8, 0);
//        mb.setBonusAt(9, 0);
//        mb.setBonusAt(10, 0);
//        mb.setChoiceAt(11, 0);
//
//        Map m = mb.build();
//        for (int i = 1; i <= 8; i++) {
//            assertTrue(m.isPlayerXAt(i, i - 1, 0));
//            assertFalse(m.hasBomb(i));
//            assertFalse(m.hasOverride(i));
//        }
//
//
//        m.setAt(8, 9, 0, Map.BONUS_BOMB);
//        //1 2 3 4 5 6 7 8 8 8 b c
//        for (int i = 1; i <= 8; i++) {
//            assertTrue(m.isPlayerXAt(i, i - 1, 0));
//            if (i == 8) {
//                assertTrue(m.hasBomb(i));
//            } else {
//                assertFalse(m.hasBomb(i));
//            }
//            assertFalse(m.hasOverride(i));
//        }
//
//        m.setAt(7, 10, 0, Map.BONUS_OVERRIDE);
//        //1 2 3 4 5 6 7 7 7 7 7 c
//        for (int i = 1; i <= 6; i++) {
//            assertTrue(m.isPlayerXAt(i, i - 1, 0));
//            assertFalse(m.hasBomb(i));
//            assertFalse(m.hasOverride(i));
//        }
//        assertEquals(1, m.getBombCountFor(8));
//        assertEquals(1, m.getOverrideCountFor(7));
//        assertTrue(m.hasOverride(7));
//
//
//        m.setAt(6, 11, 0, 8);
//        for (int i = 1; i <= 5; i++) {
//            assertTrue(m.isPlayerXAt(i, i - 1, 0));
//            assertFalse(m.hasBomb(i));
//            assertFalse(m.hasOverride(i));
//        }
//        for (int i = 5; i < 12; i++) {
//            assertTrue(m.isPlayerXAt(8, i, 0));
//        }
//        //1 2 3 4 5 8 8 8 8 8 8 8
//        assertEquals(1, m.getBombCountFor(8));
//        assertEquals(1, m.getOverrideCountFor(7));
//        assertTrue(m.hasOverride(7));
//        assertTrue(m.hasBomb(8));
//        assertFalse(m.hasBomb(6));
//        assertFalse(m.hasOverride(6));
//    }
//
//    @Test
//    public void testOverrideOnEnemy_ShouldReduceOverrideCount() {
//        //1 2 2
//        MapBuilder mb = new MapBuilder();
//        mb.setNumOfPlayers(2);
//        mb.setNumOfBombs(0);
//        mb.setNumOfOverrideStones(1);
//        mb.startNewWith(3, 1);
//        mb.setPlayerAt(1, 0, 0);
//        mb.setPlayerAt(2, 1, 0);
//        mb.setPlayerAt(2, 2, 0);
//
//        Map lmap = mb.build();
//        assertTrue(lmap.isPlayerXAt(1, 0, 0));
//        assertTrue(lmap.isPlayerXAt(2, 1, 0));
//        assertTrue(lmap.isPlayerXAt(2, 2, 0));
//        assertTrue(lmap.isValidPlayerMoveAt(1, 2, 0));
//        assertEquals(1, lmap.getOverrideCountFor(1));
//
//        lmap.setAt(1, 2, 0);
//        assertTrue(lmap.isPlayerXAt(1, 0, 0));
//        assertTrue(lmap.isPlayerXAt(1, 1, 0));
//        assertTrue(lmap.isPlayerXAt(1, 2, 0));
//        assertEquals(0, lmap.getOverrideCountFor(1));
//    }
//
//    @Test
//    public void testGetNextPlayersTurn_ShouldAvoidDisqualifiedPlayers() {
//        //1 2 3 4 5 6 7 8 x
//        MapBuilder mb = new MapBuilder();
//        mb.startNewWith(9, 1);
//        mb.setNumOfPlayers(8);
//        mb.setNumOfOverrideStones(1);
//        mb.setNumOfBombs(0);
//        for (int i = 1; i <= 8; i++) {
//            mb.setPlayerAt(i, i - 1, 0);
//        }
//        mb.setExpansionAt(8, 0);
//        Map m = mb.build();
//
//        for (int i = 1; i < 8; i++) {
//            assertEquals((i + 1), m.getPlayerAfter(i));
//        }
//        assertEquals(1, m.getPlayerAfter(8));
//    }
//
//    @Test
//    public void testNextPlayersTurnWhenNoPlayerHasMoves_ShouldReturnNoPlayer() throws Exception {
//        MapBuilder mb = new MapBuilder();
//        mb.startNewWith(1, 1);//single hole
//        mb.setNumOfPlayers(8);
//        mb.setNumOfOverrideStones(0);
//        Map m = mb.build();
//        for (int i = 1; i <= 8; i++) {
//            assertEquals(Map.NO_PLAYER_HAS_MOVE, m.getPlayerAfter(i));
//        }
//    }
//}
