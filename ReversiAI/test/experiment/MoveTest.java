package experiment;

import factories.MapFactory;
import factories.MoveFactory;
import move.Move;
import map.Map;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MoveTest {



    public MoveTest() {
    }

    public Move getMove(Map map, int player, int[] recolorCounts) {
        return null;
    }

    @Before
    public void setUp() {
    }

    @Test
    public void testBombAroundCornerAndTransitions() {
        String mapAsString = ""//
                + "8\n" //8 player
                + "1\n"//1 Override
                + "1 3\n"//1 Bomb
                + "4 6\n"// Map
                //
                + "0 0 0 0 - -\n"//
                + "0 - - x - 0\n"//
                + "0 - - - - -\n"//
                + "0 0 0 0 0 -\n"//
                //Transition
                + "4 3 7 <-> 5 1 0\n"//4 3 UP_Left 5 1 UP
                + "0 1 3 <-> 4 3 4\n"//0 1 DOWN_Right 4 3 DOWN
                ; 
      
        Map map = MapFactory.createFromString(mapAsString);
        Map copy = map.copy();
        //copy.setPhase(2);
        int player = 1;
        Move bombMove = MoveFactory.getMoveForMapUpdate(copy, player, 3, 1, 0);
        copy = bombMove.getMapAfterMove();
        // m.setAt(1, 3, 1);//bomb
        //- - - - - -
        //- - - - - -
        //- - - - - -
        //- - 0 - - -
        for (int y = 0; y < copy.getHeight(); y++) {
            for (int x = 0; x < copy.getWidth(); x++) {
                if (x == 2 && y == 3) {
                    assertTrue(copy.isEmptyAt(x, y));
                } else {
                    assertTrue(copy.isHoleAt(x, y));
                }
            }
        }

        //undo
        copy = bombMove.getMapAfterUndoMove();
        for (int y = 0; y < copy.getHeight(); y++) {
            for (int x = 0; x < copy.getWidth(); x++) {
                assertTrue(map.getChar(x, y) == copy.getChar(x, y));
            }
        }


    }
}