package move.helper;

import factories.MapFactory;
import map.Map;
import org.junit.Test;
import static org.junit.Assert.*;

public class RecolorCounterTest {

    public RecolorCounterTest() {
    }

    @Test
    public void testRecolorCountOnLoopingPath() {
        String mapAsString = ""
                + "3\n"//
                + "1\n"//
                + "1 1\n"//
                + "1 3\n"//
                + "1 2 2\n"//Map
                // Transition
                + "0 0 6 <-> 2 0 2";//0 0 Left 2 0 Right
        Map map = MapFactory.createFromString(mapAsString);
        int playerOne = 1;
        int[] recolorCounts = RecolorCounter.getForAllDirections(playerOne, map, 2, 0);
        for (int i = 0; i < 8; i++) {
            if (i != 6) {
                assertTrue(recolorCounts[i] == 0);
            } else {//left has to be 2
                assertTrue(recolorCounts[i] == 2);
            }
        }

        assertTrue(isInAllDirectionsZero(
                RecolorCounter.getForAllDirections(playerOne, map, 0, 0)));

        assertTrue(isInAllDirectionsZero(
                RecolorCounter.getForAllDirections(playerOne, map, 1, 0)));

        int playerTwo = 2;

        assertTrue(isInAllDirectionsZero(
                RecolorCounter.getForAllDirections(playerTwo, map, 0, 0)));

        assertTrue(isInAllDirectionsZero(
                RecolorCounter.getForAllDirections(playerTwo, map, 1, 0)));


        assertTrue(isInAllDirectionsZero(
                RecolorCounter.getForAllDirections(playerTwo, map, 2, 0)));


        int playerThree = 3;

        assertTrue(isInAllDirectionsZero(
                RecolorCounter.getForAllDirections(playerThree, map, 0, 0)));

        assertTrue(isInAllDirectionsZero(
                RecolorCounter.getForAllDirections(playerThree, map, 1, 0)));


        assertTrue(isInAllDirectionsZero(
                RecolorCounter.getForAllDirections(playerThree, map, 2, 0)));

    }

    private boolean isInAllDirectionsZero(int[] recolorCounts) {
        for (int i = 0; i < 8; i++) {
            if (recolorCounts[i] != 0) {
                return false;
            }
        }
        return true;
    }
}