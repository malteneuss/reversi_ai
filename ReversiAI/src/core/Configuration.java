package core;
/**
 * Stores general Informaion like which Evaluation, SearchStrategy, the kind
 * implemented Move, *  map implementation.
 */
public class Configuration {
    
    public static final int groupNumber = 10;
    //0=AllatOnce, 1=Iterative,2=AllATSorting, 3=OverrideLater, 4=noOverride, 5=iterativnoOverride
    public static int moveGenerator = 4;
    public static int evaluatorNumber = 0;//0= Simple, 1=Complex, 2=Fused
    public static int searchStrategyNumber = 1;
//      System.out.println("        0 : MiniMax");
//        System.out.println("        1 : AlphaBeta Pruning (default)");
//        System.out.println("        2 :  RandomMove Finder");
//        System.out.println("        3 : GreedyMove Finde");
//        System.out.println("        4 : AlphaBeta with MoveSorting");
    //5 AspirationWindows
    //6 MeanAlphaBeta
    public static int ownPlayerNumber = -1;
    public static int mapImplementation = 2;//1=CharMap,2=SingleCharMap
    public static final double SERVERTIME_TO_SEARCHTIME_FACTOR = 0.8;
}
