package core;

import map.Map;

public class StaticInfo {

    /**
     * Use playerAfter[currentPlayer] to get the player whose next turn it is
     * where 1<= currentPlayer <= playerCount
     */
    public static int[] playerAfter;
    public static int[] playerBefore;
    //
    /**
     * validSpecials[noBonusIndex] is an array with just 0
     * validSpecials[bonusIndex] is an array with both Bonus values
     * validSpecials[playerSwapIndex] is an array with all possible swap choices
     */
    public static int[][] validSpecials = {{0}, {Map.BONUS_BOMB, Map.BONUS_OVERRIDE}, {0}};
    public static final int noBonusIndex = 0;
    public static final int bonusIndex = 1;
    public static final int playerSwapIndex = 2;
    //
    public static int gamePhase = 1;

    public static void collectAllStaticInfo(Map map) {
        updatePlayerSwapChoices(map);
        updatePlayerBeforeAndAfterArray(map);
        updateBonusChoice();
    }

    public static void disqualify(int player) {
//        playerAfter[playerBefore[player]] = playerAfter[player];
//        playerBefore[playerAfter[player]] = playerBefore[player];
//        int[] playerSwapChoices = new int[validSpecials[playerSwapIndex].length - 1];
//        for (int i = 0, j = 0; i < playerSwapChoices.length; i++, j++) {
//            if (validSpecials[playerSwapIndex][j] == player) {
//                j++;
//            }
//            playerSwapChoices[i] = validSpecials[playerSwapIndex][j];
//        }
    }

    private static void updatePlayerSwapChoices(Map map) {
        int[] players = new int[map.getPlayerCount()];
        for (int i = 0; i < players.length; i++) {
            players[i] = i + 1;
        }
        validSpecials[playerSwapIndex] = players;
    }

    private static void updatePlayerBeforeAndAfterArray(Map map) {
        int playerCount = map.getPlayerCount();
        playerAfter = new int[playerCount + 1];
        playerBefore = new int[playerCount + 1];
        for (int current = 1; current <= playerCount; current++) {
            playerAfter[current] = (current % playerCount) + 1;
            playerBefore[current] = ((current + playerCount - 2) % playerCount) + 1;
        }
    }

    private static void updateBonusChoice() {
        int[] temp= {Map.BONUS_OVERRIDE} ; 
        validSpecials[bonusIndex] = temp;
    }
}
