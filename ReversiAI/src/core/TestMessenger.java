package core;

import ai.search.BestBombMove;
import ai.search.FastBombSearch;
import ai.search.IterativeDeepening;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import ai.search.aspirationWindows.ItDeepeningAspWindows;


import ai.search.SearchStrategy;
import ai.search.StupidBombSearch;
import factories.MapFactory;
import factories.MoveFactory;
import factories.SearchStrategyFactory;
import java.util.LinkedList;
import java.util.Queue;
import map.Map;
import map.StaticInfos;
import move.BombMove;
import move.Move;

public class TestMessenger {

    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    private Map map;
    private int ourPlayerNumber;
    private int depth;
    private long timeByServerInMs;
    public int totalMoves;
    private Queue<String> bufferedUpdateMessages;

    public TestMessenger(String adress, int port) throws IOException {
        socket = new Socket();
        socket.connect(new InetSocketAddress(adress, port), 10000);

        in = new DataInputStream(socket.getInputStream());
        out = new DataOutputStream(socket.getOutputStream());

        this.ourPlayerNumber = -1;
        this.totalMoves = 0;

        bufferedUpdateMessages = new LinkedList<>();
    }
//-------------------------------Message Type 1------------------------------------	

    public void sendGroupNumber(int groupNumber) {
        System.out.println("[Send Group number] " + Configuration.groupNumber);
        byte[] tmp = {0x01, 0x00, 0x00, 0x00, 0x01, (byte) Configuration.groupNumber};
        try {
            out.write(tmp);
            out.flush();
        } catch (IOException e) {
            System.out.println("[!!ERROR!!] Sth went wrong with sending GNumber!");
            e.printStackTrace();
        }
    }

//-------------------------------Message Type 2------------------------------------
    public void getMap() {
        String tmp = "";
        try {
            byte[] lengthTemp = new byte[4];//read length
            this.in.read(lengthTemp, 0, 4);
            int length = byteArrayToInt(lengthTemp);

            for (int i = 0; i < length; i++)//read map
            {
                tmp += (char) this.in.readByte();
            }

        } catch (IOException e) {
            System.out.println("[!!ERROR!!] Sth went wrong with recieving Map! ");
            e.printStackTrace();
        }

        this.map = MapFactory.createFromString(tmp);//create map

        StaticInfo.collectAllStaticInfo(map);
        StaticInfos.initializeInfos(map);//calculate preRating. only once needed!
    }

//-------------------------------Message Type 3------------------------------------	
    public void recievePlayerNumber() {
        try {
            byte[] lengthTemp = new byte[4];//read length
            this.in.read(lengthTemp, 0, 4);
            byteArrayToInt(lengthTemp);
            this.ourPlayerNumber = (int) this.in.readByte();
            System.out.println("[Receive Player number] " + this.ourPlayerNumber);
        } catch (IOException e) {
            System.out.println("[!!ERROR!!] Sth went wrong with recieving PlayerNumber! ");
            e.printStackTrace();
        }
    }

//-------------------------------Message Type 4------------------------------------
    public void recieveMoveRequest() {
        try {
            for (int i = 0; i < 4; i++) {
                this.in.readByte();
            }
            this.timeByServerInMs = this.in.readInt();//read timeLimit in msecs
            this.depth = (int) this.in.readByte();
            //bufferedUpdateMessages.add
            System.out.println(String.format("[Request] time: %d ms, depth: %d\n", timeByServerInMs, depth));
            if (this.depth == 0 || this.timeByServerInMs != 0)//if a timelimit is given, depth will always==0
            {
                this.depth = 15;
            }

            if (this.timeByServerInMs == 0) {
                this.timeByServerInMs = Long.MAX_VALUE;
            }

        } catch (IOException e) {
            System.out.println("[!!ERROR!!] Sth went wrong with recieving Move request! ");
            e.printStackTrace();
        }
        this.sendMove();
    }

//-------------------------------Message Type 5------------------------------------
    private void sendMove() {
        SearchStrategy search;
        if (StaticInfo.gamePhase == 1) {
            search = SearchStrategyFactory.get(map, ourPlayerNumber);
        } else {
            search = new BestBombMove(this.map, this.ourPlayerNumber);
            //search = new FastBombSearch(map, this.ourPlayerNumber);
        }
        long timeForSearch = (long) (timeByServerInMs * Configuration.SERVERTIME_TO_SEARCHTIME_FACTOR);

        Move move = search.getMove(depth, timeForSearch);
        byte[] moveMsg = moveIntoByteArray(move);
        try {//and send the move
            out.write(moveMsg);
            out.flush();
        } catch (IOException e) {
            System.out.println("[!!ERROR!!] Sth went wrong with sending a move! ");
            e.printStackTrace();
        }
        //Message created after send move to don't waste time
        String message = getLastUpdates()
                + createStatisticsMessage(search, timeForSearch, move);
        System.out.println(message);
    }

//-------------------------------Message Type 6------------------------------------
    public void recieveMadeMove() {
        byte[] xCoord = new byte[2];
        byte[] yCoord = new byte[2];
        int special = 0;
        int player = 0;
        totalMoves++;
        try {
            for (int i = 0; i < 4; i++) {
                this.in.readByte();
            }

            this.in.read(xCoord, 0, 2);
            this.in.read(yCoord, 0, 2);
            special = (int) this.in.readByte();
            player = (int) this.in.readByte();

        } catch (IOException e) {
            System.out.println("[!!ERROR!!] Sth went wrong with recieving Move a made Move! ");
            e.printStackTrace();
        }

        //Convert recieved move into system and make it on map
        int x = (int) (xCoord[1] | xCoord[0] << 8);
        int y = (int) (yCoord[1] | yCoord[0] << 8);
        if(map.getChar(x, y)=='i')
        	StaticInfos.amInversions--;
        Move move = MoveFactory.getMoveForMapUpdate(map, player, x, y, special);
        this.map = move.getMapAfterMove();
        //bufferedUpdateMessages.add
        System.out.println(String.format("[Update %d] Player %d at (%d,%d) Special: %d\n",
                totalMoves, player, x, y, special));
    }

//-------------------------------Message Type 7------------------------------------
    public void recievePlayerStatus() {
        try {
            for (int i = 0; i < 4; i++) {
                this.in.readByte();
            }
            int tempPlayer = (int) this.in.readByte();
            System.out.println(getLastUpdates() + "[Disqualification] Player: " + tempPlayer);
            if (tempPlayer == this.ourPlayerNumber) {
                System.out.println(map);
                this.socket.close();
                System.exit(0);
            }
            StaticInfo.disqualify(tempPlayer);
            
        } catch (IOException e) {
            System.out.println("[!!ERROR!!] Sth went wrong while recieving a player disq. msg! ");
            e.printStackTrace();
        }
    }

//-------------------------------Message Type 8------------------------------------
    public void recievePhase1End() {
        System.out.println(getLastUpdates() + "[Phase 1 finished] changed to Phase 2");
        StaticInfo.gamePhase = 2;
    }

//-------------------------------Message Type 9------------------------------------	
    public void recievePhase2End() {
        System.out.println(getLastUpdates() + "[Phase 2 finished] We were player " + this.ourPlayerNumber);
        try {
            this.socket.close();
            System.exit(0);
        } catch (IOException e) {
            System.out.println("[!!ERROR!!] couldnt close connection after end of phase2! ");
            e.printStackTrace();
        }
    }

    public void doIt(int msgType) {
        if (msgType == 2) {
            this.getMap();
        }
        if (msgType == 3) {
            this.recievePlayerNumber();
        }
        if (msgType == 4) {
            this.recieveMoveRequest();
        }
        if (msgType == 6) {
            this.recieveMadeMove();
        }
        if (msgType == 7) {
            this.recievePlayerStatus();
        }
        if (msgType == 8) {
            this.recievePhase1End();
        }
        if (msgType == 9) {
            this.recievePhase2End();
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {

        TestMessenger messenger = new TestMessenger(args[0], Integer.parseInt(args[1]));
        int messageNumber;
        messenger.sendGroupNumber(10);

        while (true) {
            messageNumber = (int) messenger.in.readByte();
            messenger.doIt((int) messageNumber);//and forward it to get reacted to
        }
    }

    public static int byteArrayToInt(byte[] b) {
        return b[3] & 0xFF | (b[2] & 0xFF) << 8 | (b[1] & 0xFF) << 16 | (b[0] & 0xFF) << 24;
    }

    public static byte[] intToByteArray(int val) {
        return new byte[]{(byte) ((val >> 24) & 0xFF), (byte) ((val >> 16) & 0xFF), (byte) ((val >> 8) & 0xFF), (byte) (val & 0xFF)};
    }

    private byte[] moveIntoByteArray(Move move) {
        byte[] moveMsg = new byte[10];//convert move for byte-stream
        moveMsg[0] = 0x5;//msgType
        moveMsg[1] = 0x00;//Length
        moveMsg[2] = 0x00;
        moveMsg[3] = 0x00;
        moveMsg[4] = 0x5;
        byte[] xCoord = {(byte) ((move.getX() >> 8) & 0xFF), (byte) (move.getX() & 0xFF)};
        byte[] yCoord = {(byte) ((move.getY() >> 8) & 0xFF), (byte) (move.getY() & 0xFF)};
//
        moveMsg[5] = xCoord[0];
        moveMsg[6] = xCoord[1];
//
        moveMsg[7] = yCoord[0];
        moveMsg[8] = yCoord[1];
        moveMsg[9] = (byte) move.getSpecial();
        return moveMsg;
    }

    private String createStatisticsMessage(SearchStrategy search, long timeForSearch, Move move) {
        String message = "-----------------------------------\n"
                + "[Info] Statistics for player: " + this.ourPlayerNumber
                + String.format("\n[Info] Started '%s' with \n[Info] time: %d ms, depth: %d\n", search.getName(), timeForSearch, depth)
                + ((search instanceof ItDeepeningAspWindows)
                ? "[Info] ID finished depth " + ((ItDeepeningAspWindows) search).getFinishedDepth() + "\n"
                : "")
                + ((search instanceof IterativeDeepening)
                ? "[Info] ID finished depth " + ((IterativeDeepening) search).getFinishedDepth() + "\n"
                : "")
                + String.format("[Info] Nodes: %d, Evaluations: %d\n", search.getNodeCount(), search.getEvalCount())
                + "[Move Answer] " + move.toString()
                + "\n-----------------------------------\n";
        return message;
    }

    private String getLastUpdates() {
        String updates = "";
        while (!bufferedUpdateMessages.isEmpty()) {
            updates += bufferedUpdateMessages.remove();
        }
        return updates;
    }
}
