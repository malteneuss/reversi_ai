package core;

import java.io.IOException;

public class Main {

    public static void main(String args[]) throws IOException, InterruptedException{

       
        //TODO Parameter fuer alphaBeta oder normal
        
        String[] address = {"127.0.0.1", "7777"};
        
        boolean wasPrint = false;
        
        for(int i= 0; i < args.length; i++){
            
            if(args[i].equals("-e")){ // specific evaluator
                if(i < args.length-1){
                    Configuration.evaluatorNumber = Integer.parseInt(args[i+1]);
                    System.out.println("With a specific Evaluator");
                    i++;
                }
                else{
                    printHelp();
                    wasPrint = true;
                }
            }
            else if(args[i].equals("-s")){ // specific searchstrategy
                if(i < args.length-1){
                    Configuration.searchStrategyNumber = Integer.parseInt(args[i+1]);
                    System.out.println("With a specific Searchstrategy");
                    i++;
                }
                else{
                    printHelp();
                    wasPrint = true;
                }
            }
            else if(args[i].equals("-mg")){
                if(i < args.length){
                    
                    Configuration.moveGenerator = Integer.parseInt(args[i+1]);
                    System.out.println("With a specific MoveGenerator");
                    i++;
                }
                else {
                    printHelp();
                    wasPrint = true;
                }
            }
            else{
                if(i < args.length -1){
                    System.out.println("Connecting to specific Server");
                    address[0] = args[i];
                    address[1] = args[i+1];
                    i++;
                }
                else{
                    printHelp();
                    wasPrint = true;
                }
            }
        }
        if(!wasPrint)
            TestMessenger.main(address);
                
    }
    
    public static void printHelp(){
        System.out.println("Parameter order: -e <number> -s <number> -mg <number> [Hostaddress] [Hostport]");
        System.out.println("-e   < 0 , 1 > : Set the Evaluation-strategy");
        System.out.println("        0 :  SimplyParanoidEvaluator");
        System.out.println("        1 :  ComplexEvaluation (default)");   
        System.out.println("-s   < 0 , 1 , 2 , 3 , 4 > : Set the Search-strategy");
        System.out.println("        0 : MiniMax");          
        System.out.println("        1 : AlphaBeta Pruning (default)"); 
        System.out.println("        2 :  RandomMove Finder");
        System.out.println("        3 : GreedyMove Finde");
        System.out.println("        4 : AlphaBeta with MoveSorting");
        System.out.println("-mg  < 0 , 1 > : Sets the MoveGenerator typ");
        System.out.println("        0 : all moves are generated at once (default)");
        System.out.println("        1 : moves are generated step by step");
    }
}
