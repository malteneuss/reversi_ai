package map;

public class CharMap extends Map {

    private char[][] tiles;

    public CharMap(int width, int height) {
        super.width = width;
        super.height = height;
        tiles = new char[height][width];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                tiles[y][x] = HOLE;
            }
        }
        super.createEmptyTransitionField();
    }

    private CharMap(CharMap source) {
        super.width = source.width;
        super.height = source.height;
        char[][] tilesCopy = new char[height][width];
        for (int y = 0; y < height; y++) {
            System.arraycopy(source.tiles[y], 0, tilesCopy[y], 0, width);
        }
        this.tiles = tilesCopy;
        super.transitions = source.transitions;
        super.playerCount = source.playerCount;
        super.bombStrength = source.bombStrength;

        super.bombCountOf = source.copyOfBombCounts();
        super.overrideCountOf = source.copyOfOverrideCounts();
    }

    @Override
    public void setChar(char c, int x, int y) {
        tiles[y][x] = c;
    }

    @Override
    public char getChar(int x, int y) {
        return tiles[y][x];
    }

    @Override
    public Map copy() {
        return new CharMap(this);
    }

    public static void main(String[] args) {
        CharMap map = new CharMap(2, 2);
        map.setPlayerCount(8);
        map.setStartBombCount(3);
        map.setStartOverrideCount(4);
        map.setBombStrength(6);

        map.setPlayerAt(1, 0, 0);
        System.out.println(map.toString());
    }
}
