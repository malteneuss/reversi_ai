package map;

import factories.MapFactory;
import ai.evaluation.experiment.ComplexEvaluation;
import map.Map;


public class StaticInfos {

  //  public static StaticInfos instance;
	private static int[][] preratioOfMap;//the static values for each tile(used for evaluation)
	private static Map map;

	public static int amInversions=0;
	public static int amBonus=0;
	public static int amChoice=0;
	public static int amExtensions=0;
	public static int amHoles=0;
	public static int amPlayableFields=0;
	public static int bombRating=0;
	public static int inversionRating;
	public static int choiceRating;
	public static  int[][] borders;//borders of the chosen quadrant(s) for bombMoves
	public static void initializeInfos(Map m) {
		map=m;
		preRateMap();
		borders=getBorders(map);
		bombRating*=m.getBombStrength();
	}
	/**
     * Prerates the map. for each tile a value is calculated how important it would be to make a move on that tile 
     * Only needs to be calculated once
     */
    private static void preRateMap() {
    	preratioOfMap=new int[map.getWidth()][map.getHeight()];
    	final int height=map.getHeight(),width=map.getWidth();
    	
    	for(int i=0;i<width;i++) {
    		for(int j=0;j<height;j++) {
    			preratioOfMap[i][j]=prerateTile(i,j,map);
    			
    	    	final char fieldValue=map.getChar(i, j);
    	    	if(fieldValue=='b')
    	    		amBonus++;
    	    	else if(fieldValue=='c')
    	    		amChoice++;
    	    	else if(fieldValue=='i')
    	    		amInversions++;
    	    	else if(fieldValue=='x')
    	    		amExtensions++;
    	    	else if(fieldValue=='-') {
    	    		amHoles++;
    	    		continue;
    	    	}
    	    	amPlayableFields++;	
    	    }
    	}
    }
    
	/**
     * Prerates a tile. a value is calculated how important it would be to make a move on that tile 
     * @param x the x coordinate
     * @param y the y coordinate
     * @param copyOfMap the map
     * @return a value representing the importance of that tile
     */
    private static int prerateTile(final int x, final int y, final Map copyOfMap) {
 		int res=0;
 		int[] buffer;
 		int amountOfEdgesOneFieldAway=0;
 		int amountOfEdgesTwoFieldsAway=0;
 		int newX=x,newY=y;
 
 		if(map.getChar(x,y)=='-')
 			return res=0;
 		
 		if(map.getChar(x,y)=='b')
 			res+=5000;
 		
 		for(int dir=0;dir<=7;dir++) {//check all directions around tile for edge/corners
 			buffer=ComplexEvaluation.goIntoDirection(newX,newY,dir);
 		
 			if(outOfMap(buffer[1],buffer[0],copyOfMap)||map.getChar(buffer[1],buffer[0])=='-')//one Field away in that direction
 				amountOfEdgesOneFieldAway++;	

 			else {//2 fields away
 				newX=buffer[0];
 				newY=buffer[1];
 				buffer=ComplexEvaluation.goIntoDirection(newX,newY,dir);
 				if(outOfMap(buffer[0],buffer[1],copyOfMap)||map.getChar(buffer[0],buffer[1])=='-')
 	 				amountOfEdgesTwoFieldsAway++;
 			}
 			newX=x;
 			newY=y;
 		}
 		if(amountOfEdgesOneFieldAway==0&&amountOfEdgesTwoFieldsAway==0)
 			return res+1;
 		res+=10*amountOfEdgesOneFieldAway-5*amountOfEdgesTwoFieldsAway; //each border which cant be claimed brings this field one step closer to a safe stone(5 directions denied=corner)	
 		return res;	
 	}
    
    public static boolean outOfMap(final int x,final int y, Map copyOfMap) {
 		//System.out.println("oomCheck: x "+x+"y "+y);
 		return (x<0)||(y<0)||(x>(copyOfMap.getWidth()-1))||(y>(copyOfMap.getHeight()-1));
    }
    
    /**
     * returns the prerating of a Tile. The rating is based how good/bad it would be to set a stone on it during a move
     * @param x the x coordinate
     * @param y the y coordinate
     * @return returns a value depending on the location and properties of that tile
     */
    public static int getTilePreRating(final int x,final int y) {
    	return preratioOfMap[x][y];
    }
   
    private static int[][] getBorders(Map m) {
		int w=m.getWidth(),h=m.getHeight();//calculate borders
		int[][] borders=new int[9][4];
		borders[0]=new int[]{0, w/2, 0, h/2};//q0
		borders[1]=new int[]{(w/4), (3*w)/4, 0, h/2};//q01
		borders[2]=new int[]{w/2, w-1, 0, h/2};//q1
		
		borders[3]=new int[]{0, w/2, (h/4), (3*h)/4};//q02
		borders[4]=new int[]{((w)/4), (3*w)/4, (h/4), (3*h)/4};//q0123
		borders[5]=new int[]{w/2, w-1, (h/4), (3*h)/4};//q13
		
		borders[6]=new int[]{0, w/2, (h/2), h-1};//q2
		borders[7]=new int[]{(w/4), (3*w)/4, (h/2), h-1};//q23
		borders[8]=new int[]{w/2, w-1, (h/2), h-1};//q3
		return borders;
	}	
    
	public static void main(String[] args) {
		String s="./maps/phase1/RoundedMap.map";
		Map m = MapFactory.createFromFile(s);
		//StaticInfos sI=new StaticInfos(m);
		for(int y=0;y<m.getHeight();y++) {
			for(int x=0;x<m.getWidth();x++) {
				System.out.print(StaticInfos.getTilePreRating(x, y)+" ");
			}
			System.out.println();
		}
		System.out.println("i: "+StaticInfos.amInversions+" c:"+StaticInfos.amChoice);
		System.out.println("b: "+StaticInfos.amBonus+" x:"+StaticInfos.amExtensions);
		System.out.println("holes: "+StaticInfos.amHoles+" playableFields:"+StaticInfos.amPlayableFields);
	}

}
