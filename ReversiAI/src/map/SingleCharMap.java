package map;

import static map.Map.HOLE;

public class SingleCharMap extends Map {

    private char[] tiles;

    public SingleCharMap(int width, int height) {
        super.width = width;
        super.height = height;
        tiles = new char[height * width];
        for (int i = 0; i < tiles.length; i++) {
            tiles[i] = HOLE;
        }

        super.createEmptyTransitionField();
    }

    private SingleCharMap(SingleCharMap source) {
        super.width = source.width;
        super.height = source.height;
        char[] tilesCopy = new char[source.tiles.length];
        System.arraycopy(source.tiles, 0, tilesCopy, 0, source.tiles.length);

        this.tiles = tilesCopy;
        super.transitions = source.transitions;
        super.playerCount = source.playerCount;
        super.bombStrength = source.bombStrength;

        super.bombCountOf = source.copyOfBombCounts();
        super.overrideCountOf = source.copyOfOverrideCounts();
    }

    @Override
    public void setChar(char c, int x, int y) {
        tiles[(y * width) + x] = c;
    }

    @Override
    public char getChar(int x, int y) {
        return tiles[(y * width) + x];
    }

    @Override
    public Map copy() {
        return new SingleCharMap(this);
    }

    //TODO override swap,inverse,reverse

    public static void main(String[] args) {
        SingleCharMap map = new SingleCharMap(2, 2);
        map.setPlayerCount(8);
        map.setStartBombCount(3);
        map.setStartOverrideCount(4);
        map.setBombStrength(6);

        map.setPlayerAt(1, 0, 0);
        System.out.println(map.toString());
    }
}
