package map;

import core.StaticInfo;

public abstract class Map {

    public static final int BONUS_BOMB = 20;
    public static final int BONUS_OVERRIDE = 21;
    //
    public static final char HOLE = '-';
    public static final char EMPTY = '0';
    public static final char INVERSION = 'i';
    public static final char BONUS = 'b';
    public static final char CHOICE = 'c';
    public static final char EXPANSION = 'x';
    public static final char MARKED = 'm';
    public static final int PLAYER_TO_CHAR_OFFSET = 48;
    //
    protected int width, height;
    protected int playerCount;
    protected int[] bombCountOf, overrideCountOf;
    protected int bombStrength;
    protected int[][][][] transitions;
    //
    // protected int[] stoneCountOf;

    public abstract void setChar(char c, int x, int y);

    public abstract char getChar(int x, int y);

    public abstract Map copy();

    //creating
    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }

    public void setBombStrength(int strength) {
        this.bombStrength = strength;
    }

    public void setStartBombCount(int bombCount) {
        if (playerCount == 0) {
            throw new RuntimeException("Define playerCount first");
        }
        bombCountOf = new int[playerCount];
        for (int i = 0; i < playerCount; i++) {
            bombCountOf[i] = bombCount;
        }
    }

    public void setStartOverrideCount(int overrideCount) {
        if (playerCount == 0) {
            throw new RuntimeException("Define playerCount first");
        }
        overrideCountOf = new int[playerCount];
        for (int i = 0; i < playerCount; i++) {
            overrideCountOf[i] = overrideCount;
        }
    }
    ///
    /**
     * Use direction[direction] to get x and y offsets, direction 0 to 7
     */
    private static int[][] directionXY = {{0, -1}, {1, -1}, {1, 0}, {1, 1}, {0, 1}, {-1, 1}, {-1, 0}, {-1, -1}};
   
    /**
     * Computes the resulting position and direction by starting at (x,y) and
     * going into a direction. Takes transitions into account.
     *
     * @param pd pd[0]=x, pd[1]=y, pd[2]=direction, CAUTION: input variable will
     * be modified
     * @return is null if resulting position on hole or out of the map
     */
    public int[] goOneStepInLineAlong(int[] pd) {
        assert (pd != null);
        int x = pd[0];
        int y = pd[1];
        int direction = pd[2];
        int newX = x + directionXY[direction][0];//directions is Used as an Index
        int newY = y + directionXY[direction][1];
        if (isOnMap(newX, newY) && !isHoleAt(newX, newY)) {//no transition possible
            pd[0] = newX;
            pd[1] = newY;
            return pd;         
        } else {//if out of map try for transition
            int[] endpoint = getTransitionEndpoint(x, y, direction);
            if (endpoint != null) {
                System.arraycopy(endpoint, 0, pd, 0, 3);
                return pd;
            }
        }
        return null;
    }

    public void swap(int player1, int player2) {
        char p1 = (char) (player1 + PLAYER_TO_CHAR_OFFSET);
        char p2 = (char) (player2 + PLAYER_TO_CHAR_OFFSET);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                char tile = getChar(x, y);
                if (tile == p1) {
                    setChar(p2, x, y);
                } else if (tile == p2) {
                    setChar(p1, x, y);
                }
            }
        }
    }

    public void inverse() {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int currentPlayer = getChar(x, y) - PLAYER_TO_CHAR_OFFSET;
                if (currentPlayer < 1 || currentPlayer > playerCount) {
                    continue;
                }
                setPlayerAt(StaticInfo.playerAfter[currentPlayer], x, y);
            }
        }
    }

    public void reverse() {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int currentPlayer = getChar(x, y) - PLAYER_TO_CHAR_OFFSET;
                if (currentPlayer < 1 || currentPlayer > playerCount) {
                    continue;
                }
                setPlayerAt(StaticInfo.playerBefore[currentPlayer], x, y);
            }
        }
    }

    public int getPlayerCount() {
        return this.playerCount;
    }

    public void setPlayerAt(int player, int x, int y) {
        setChar((char) (player + PLAYER_TO_CHAR_OFFSET), x, y);
    }

    public boolean isPlayerAt(int player, int x, int y) {
        return getChar(x, y) == player + PLAYER_TO_CHAR_OFFSET;
    }

    public int getBombCountOf(int player) {
        return bombCountOf[player - 1];
    }

    public void setBombCountOf(int player, int value) {
        bombCountOf[player - 1] = value;
    }

    public int getOverrideCountOf(int player) {
        return overrideCountOf[player - 1];
    }

    public void setOverrideCountOf(int player, int value) {
        overrideCountOf[player - 1] = value;
    }

    public int getBombStrength() {
        return bombStrength;
    }

    /**
     * Adds Positiondirection (x2,y2,opposite(d2)) to (x1,y1,d1) for faster
     * retrieval, and the other way around
     */
    public void addTransition(int x1, int y1, int d1, int x2, int y2, int d2) {
        transitions[x1][y1][d1] = createPositionDirectionFrom(x2, y2, oppositeDirectionOf(d2));
        transitions[x2][y2][d2] = createPositionDirectionFrom(x1, y1, oppositeDirectionOf(d1));
    }

    /**
     *
     * @return int[3] with x, y and direction, returns the other endpoint of a
     * transition. Caution!: direction is already * facing away from transition
     * endpoint. Returns null if no such transition.
     */
    public int[] getTransitionEndpoint(int x, int y, int direction) {
        return transitions[x][y][direction];
    }

    public int getOverrideAmOf(int player) {
        return this.overrideCountOf[player - 1];
    }

    private int[] createPositionDirectionFrom(int x, int y, int direction) {
        int[] positionDirection = new int[3];
        positionDirection[0] = x;
        positionDirection[1] = y;
        positionDirection[2] = direction;
        return positionDirection;
    }

    private int oppositeDirectionOf(int direction) {
        return (direction + 4) % 8;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    /**
     * A Tile is called free for placing a stone if it is empty or a
     * bonus/choice /inversion tile
     */
    public boolean isFree(int x, int y) {
        char tile = getChar(x, y);
        return tile == EMPTY || tile == BONUS || tile == CHOICE || tile == INVERSION;
    }

    public boolean isHoleAt(int x, int y) {
        return getChar(x, y) == HOLE;
    }

    public boolean isOnMap(int x, int y) {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    public boolean isBombableAt(int x, int y) {
        return getChar(x, y) != HOLE;
    }

    public boolean isOverrideableAt(int x, int y) {
        return getChar(x, y) == EXPANSION || isAnyPlayerAt(x, y);
    }

    public boolean isAnyPlayerAt(int x, int y) {
        int player = getChar(x, y) - PLAYER_TO_CHAR_OFFSET;
        return (player >= 1 && player <= playerCount);
    }

    public boolean isExpansionAt(int x, int y) {
        return getChar(x, y) == EXPANSION;
    }

    public boolean isEmptyAt(int x, int y) {
        return getChar(x, y) == EMPTY;
    }

    public boolean isBonusAt(int x, int y) {
        return getChar(x, y) == BONUS;
    }

    public boolean isChoiceAt(int x, int y) {
        return getChar(x, y) == CHOICE;
    }

    public boolean isInversionAt(int x, int y) {
        return getChar(x, y) == INVERSION;
    }

//    public int[] getStoneCountOfAll() {
//        return stoneCountOf;
//    }

    protected void createEmptyTransitionField() {
        if (width == 0 || height == 0) {
            throw new RuntimeException("Define widht and height first");
        }
        transitions = new int[width][height][8][];
    }

    protected int[] copyOfOverrideCounts() {
        int[] copy = new int[playerCount];
        System.arraycopy(overrideCountOf, 0, copy, 0, playerCount);
        return copy;
    }

    protected int[] copyOfBombCounts() {
        int[] copy = new int[playerCount];
        System.arraycopy(bombCountOf, 0, copy, 0, playerCount);
        return copy;
    }

    @Override
    public String toString() {
        String mapAsString = "-----------------------------\n"
                + "Map name\nPlayerCount=" + playerCount
                + "\nWidth=" + width + ", Height=" + height
                + "\nBombStrength=" + bombStrength + "\n"
                + transitionsToString()
                + playerStatusToString()
                + tilesToString();
        return mapAsString;
    }

    protected String playerStatusToString() {
        String playerStatus = "";
        for (int i = 0; i < playerCount; i++) {
            playerStatus += "Player " + (i + 1) + ":";
            playerStatus += " Bombs: " + bombCountOf[i];
            playerStatus += " Overrides: " + overrideCountOf[i];
            playerStatus += "\n";
        }
        playerStatus += "\n";
        return playerStatus;
    }

    protected String tilesToString() {
        String tileString = "";
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                tileString += getChar(x, y) + " ";
            }
            tileString += "\n";
        }
        return tileString;
    }

    protected String transitionsToString() {
        String transitionsAsString = "Transitions (all appear twice):\n";
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int d = 0; d < 8; d++) {
                    int[] end = getTransitionEndpoint(x, y, d);
                    if (end != null) {
                        transitionsAsString += String.format("%d %d %d <-> %d %d %d\n",
                                x, y, d, end[0], end[1], oppositeDirectionOf(end[2]));
                    }
                }
            }
        }
        return transitionsAsString;
    }
}
