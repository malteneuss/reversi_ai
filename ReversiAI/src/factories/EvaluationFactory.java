package factories;

import ai.evaluation.Evaluation;
import ai.evaluation.FastSimpleEvaluation;
import ai.evaluation.SimpleEvaluation;
import ai.evaluation.experiment.FusedEvaluation;
import ai.evaluation.experiment.NewEvaluation;
import core.Configuration;

/**
 * Generates Evaluatoin specified in the Configuration
 */
public class EvaluationFactory {

    public static Evaluation get() {
        switch (Configuration.evaluatorNumber) {
            case 0:
                return new FastSimpleEvaluation();
            case 1:
                return new NewEvaluation();
            case 2:
                return new FusedEvaluation();
            default:
                return new SimpleEvaluation();
        }
    }
}
