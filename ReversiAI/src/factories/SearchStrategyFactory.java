package factories;

import ai.evaluation.Evaluation;
import ai.search.AlphaBeta;
import ai.search.AlphaBetaMeanInversion;
import ai.search.GreedySearch;
import ai.search.IterativeDeepening;
import ai.search.MiniMax;
import ai.search.RandomSearch;
import ai.search.SearchStrategy;
import ai.search.SortingAlphaBeta;
import ai.search.aspirationWindows.ItDeepeningAspWindows;
import core.Configuration;
import map.Map;

public class SearchStrategyFactory {

    /**
     * Returns "low level" search algorithm, to be used with Iterative Deepening
     * or without
     */
    public static SearchStrategy get(Map map, int player) {
        Evaluation e = EvaluationFactory.get();
        switch (Configuration.searchStrategyNumber) {
            case 0:
                return new IterativeDeepening(new MiniMax(map, player, e));
            case 1:
                return new IterativeDeepening(new AlphaBeta(map, player, e));
            case 2:
                return new IterativeDeepening(new RandomSearch(map, player, e));
            case 3:
                return new IterativeDeepening(new GreedySearch(map, player, e));
            case 4:
                return new IterativeDeepening(new SortingAlphaBeta(map, player, e));
            case 5:
                return new ItDeepeningAspWindows(map, player, e);
            case 6:
                return new AlphaBetaMeanInversion(map, player, e);
            default:
                return new IterativeDeepening(new AlphaBeta(map, player, e));
        }
    }

}
