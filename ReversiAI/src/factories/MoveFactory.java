package factories;

import core.StaticInfo;
import java.util.List;
import move.CopyingStoneMove;
import move.Move;
import move.helper.RecolorCounter;
import map.Map;
import move.BombMove;
import move.InvalidMove;
import move.UndoStoneMove;
import move.helper.TilePosition;

public class MoveFactory {

   
    public static Move getRegularMoveFrom(Map map, int player, int x, int y, int special, int[] recolorCounts) {
        return new CopyingStoneMove(map, player, x, y, special, recolorCounts);
    }

    /**
     * a variant for simple positionlists instead of recolorCount array for all
     * directions
     *
     * @param recolorPositions It is not allowed to include (x,y)
     * @return
     */
    public static Move getRegularMoveFrom(Map map, int player, int x, int y, int special, List<TilePosition> recolorPositions) {
        return new UndoStoneMove(map, player, x, y, special, recolorPositions);
    }

    public static Move getBombMoveFrom(Map map, int player, int x, int y) {
        return new BombMove(map, player, x, y);
    }

    /**
     * Must be a valid move on given map, map updates from server are always
     * valid
     */
    public static Move getMoveForMapUpdate(Map map, int player, int x, int y, int special) {
        if (StaticInfo.gamePhase == 1) {
            //expensive but doesn't matter
            int[] recolorCounts = RecolorCounter.getForAllDirections(player, map, x, y);
            return getRegularMoveFrom(map, player, x, y, special, recolorCounts);
        } else {
            return getBombMoveFrom(map, player, x, y);
        }
    }

    public static Move getInvalidMove(int rating) {
        return new InvalidMove(rating);
    }
}
