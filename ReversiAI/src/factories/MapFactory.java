package factories;

import core.Configuration;
import core.StaticInfo;
import map.CharMap;
import map.Map;
import map.StaticInfos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import map.SingleCharMap;

public class MapFactory {

    public static Map getFrom(int width, int height) {
        switch (Configuration.mapImplementation) {
            case 1:
                return new CharMap(width, height);
            case 2:
                return new SingleCharMap(width, height);
            default:
                System.out.println("Used default Map implementation");
                return new CharMap(width, height);
        }
    }

    public static Map createFromFile(String pathToMap) {
        String mapString = "";
        String buffer;
        try {
            BufferedReader fr = new BufferedReader(new FileReader(pathToMap));
            while ((buffer = fr.readLine()) != System.getProperty("line.separator")) {
                if (buffer == null) {
                    break;
                }
                mapString += (buffer + "\n");
            }

            fr.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MapFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MapFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return createFromString(mapString);
    }

    public static Map createFromString(String mapAsString) {
        String[] lines = mapAsString.split("\r?\n");

        //Line 4 height and width
        String[] heightWidthAsString = lines[3].split(" ");
        int height = (Integer.parseInt(heightWidthAsString[0]));
        int width = (Integer.parseInt(heightWidthAsString[1]));
        //
        Map map = getFrom(width, height);

        //Line 1 numofPlayers
        map.setPlayerCount(Integer.parseInt(lines[0].trim()));

        //Line 2 numOfOverrides
        map.setStartOverrideCount(Integer.parseInt(lines[1].trim()));

        //Line 3 bombCount and Strenght/radius
        String[] bombsNStrength = lines[2].split(" ");
        map.setStartBombCount(Integer.parseInt(bombsNStrength[0]));
        map.setBombStrength(Integer.parseInt(bombsNStrength[1].trim()));

        //Lines 5 to 5 + height => tiles
        String tilesRow;
        for (int y = 0; y < height; y++) {
            tilesRow = lines[y + 4];
            addRow(tilesRow, map, y);
        }

        //remaining parts are transitions
        for (int i = height + 4; i < lines.length; i++) {
            String[] transitionAsString = lines[i].split(" ");//transition
            int x1 = Integer.parseInt(transitionAsString[0]);
            int y1 = Integer.parseInt(transitionAsString[1]);
            int d1 = Integer.parseInt(transitionAsString[2]);
            //ignore [3] because it is "<->"
            int x2 = Integer.parseInt(transitionAsString[4]);
            int y2 = Integer.parseInt(transitionAsString[5]);
            int d2 = Integer.parseInt(transitionAsString[6]);

            map.addTransition(x1, y1, d1, x2, y2, d2);
        }
        StaticInfos.initializeInfos(map);
        StaticInfo.collectAllStaticInfo(map);
        return map.copy();
    }

    private static void addRow(String tilesRow, Map map, int y) {
        tilesRow = tilesRow.trim();
        int width = (tilesRow.length() / 2) + 1;//1 1\n, \n doesn't count, so it is always odd
        for (int x = 0; x < width; x++) {
            char tile = tilesRow.charAt(x * 2);
            map.setChar(tile, x, y);
        }
    }

    public static void main(String[] args) {
        Map map = createFromFile("./maps/map03.map");
        System.out.println(map.toString());
    }
  
}
