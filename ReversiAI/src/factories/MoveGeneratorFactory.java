package factories;

import core.Configuration;
import core.StaticInfo;
import movegenerator.AllAtOnceMoveGenerator;
import movegenerator.IterativeMoveGenerator;
import movegenerator.MoveGenerator;
import map.Map;
import movegenerator.AllAtOnceSortedGenerator;
import movegenerator.AllAtOnceUndoMoveGenerator;
import movegenerator.AllOverrideJustWhenNecessaryGenerator;
import movegenerator.AllOverrideLaterGenerator;
import movegenerator.IterativeOverrideJustWhenNecessaryGenerator;

/**
 * Generates specific move Generators defined by the Configurarion.
 */

public class MoveGeneratorFactory {

    public static MoveGenerator getFrom(Map map, int player) {
        if (StaticInfo.gamePhase != 1) {
            throw new UnsupportedOperationException("GeneratorFactory not for Bombmoves yet");
        }
        switch (Configuration.moveGenerator) {
            case 0:
                return new AllAtOnceMoveGenerator(map, player);
            case 1:
                return new IterativeMoveGenerator(map, player);
            case 2:
                return new AllAtOnceSortedGenerator(map, player);
            case 3:
                return new AllOverrideLaterGenerator(map, player);
            case 4:
                return new AllOverrideJustWhenNecessaryGenerator(map, player);
            case 5:
                return new IterativeOverrideJustWhenNecessaryGenerator(map, player);
            default:
                return new AllAtOnceMoveGenerator(map, player);
        }             
    }
}
