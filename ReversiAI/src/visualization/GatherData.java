package visualization;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


import ai.search.AlphaBeta;
import ai.search.MiniMax;

import ai.evaluation.Evaluation;
import ai.evaluation.SimpleEvaluation;
import ai.evaluation.experiment.ComplexEvaluation;
import ai.search.IterativeDeepening;

import ai.search.SearchStrategy;
import ai.search.SortingAlphaBeta;
import ai.search.aspirationWindows.ItDeepeningAspWindows;

import factories.MapFactory;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import map.Map;

public class GatherData {

    private static DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");

    public static long timeLimitInMilliSec = 50000;
    public static int maxSearchDepth = 10;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
//----------------------Example how to use the logger---------------------------------------------
            String[] paths = {
                "./maps/minimal.map",
                "./maps/map03.map",
                "./maps/map3_midgame.map",
                "./maps/map3_endgame(352_~394).map",
                "./maps/6PlayerMean.map",
                "./maps/map6_midgame.map",
                "./maps/map6_endgame(920_~1020).map",
                "./maps/8-player FUU.map",  
                "./maps/map8_midgame.map", 
                "./maps/map8_endgame(1609_~1740).map",
                };

            GatherData.gatherData(paths, maxSearchDepth, timeLimitInMilliSec);
	}
	
    public static void gatherData(String[] maps, int depth, long duration) {
        String date = dateFormat.format(new Date());
		long durInMicroSecs=0;
            String path = "";
		String[] output=new String[500];
		for(String s : maps) {
			path=s.substring(7);
                    Map m = MapFactory.createFromFile(s);
            Evaluation evaluator = new ComplexEvaluation();//
            //ComplexEvaluation(m);
                    SearchStrategy[] searchStrategies = {
                    		new ItDeepeningAspWindows(m, 1, evaluator,10),
                    		new ItDeepeningAspWindows(m, 1, evaluator,50),
                    		new ItDeepeningAspWindows(m, 1, evaluator,100),
                    		new ItDeepeningAspWindows(m, 1, evaluator,150),
                    		new ItDeepeningAspWindows(m, 1, evaluator,200)
                    };//,new SortingAlphaBeta(m, 1, evaluator)};
			
                    output[0] = "map: " + path + " Timelimit: " + ((double)duration)/1000;
			int i=1;
                    
                    for (int l = 0; l < searchStrategies.length; l++) {
                        output[i] = searchStrategies[l].getName();
                           
                            
				i++;
                            
				for(int d=1;d<=depth;d++) {
					long start = System.nanoTime();
                                    searchStrategies[l].getMove(d, duration);
					durInMicroSecs = (System.nanoTime()-start) / 1000;
		    
                                    output[i] = String.format("%d %d %d %d %f %d",((ItDeepeningAspWindows)searchStrategies[l]).getRestartCount(), d, searchStrategies[l].getNodeCount(), durInMicroSecs, ((double) durInMicroSecs) * 0.000001, searchStrategies[l].getEvalCount());
					i++;
					//GatherData.writeIntoFile(output, "./runTimeData/data_"+path+".txt");
				}
                            i++;
                            GatherData.writeIntoFile(output, "./runTimeData/" + date+path + ".txt");
			}
	       	
		}

	}
	
	private static void deleteLogs() {
		File files=new File("./runTimeData/");
		if(files.isDirectory()) {
			for(File f : files.listFiles()) 
				f.delete();
		}
	}
	
	private  static void writeIntoFile(String[] info,String path)  {
		try {
    		PrintWriter pW=new PrintWriter(new FileWriter(path),true);
    		for(int i=0;i<info.length;i++) {
    			if(info[i]==null)
    				continue;
    			pW.append(info[i]);
    			pW.append(System.getProperty("line.separator"));
    			pW.flush();
    		}
    		pW.close();
    	}
    	catch(IOException iO) {
    		iO.printStackTrace();
    	}
    }
}