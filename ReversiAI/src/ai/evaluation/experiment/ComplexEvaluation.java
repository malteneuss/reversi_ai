package ai.evaluation.experiment;



import ai.evaluation.Evaluation;
import factories.MapFactory;
import factories.MoveGeneratorFactory;
import move.Move;
import movegenerator.MoveGenerator;
import map.Map;
import map.StaticInfos;

public class ComplexEvaluation implements Evaluation {
	private Map map;
	private int playerToEvalFor;
//TODO get amount of playableFields and stonesOnField with prerating	
	private int playableFields;
	private int stonesOnField;
	private int[] amountOfStones;// pos (i-1)= how many stones got player i on field
	
	private MoveGenerator moves;

	public static int[] weights;//weigths.length=amount of rating criterias. assigns a value to each criteria on how important it is(can change during game). sum is 100!!!
	public ComplexEvaluation() {}
	public void initiate(Map map,int player) {
		this.map=map;
		this.playerToEvalFor=player;
		
		moves=MoveGeneratorFactory.getFrom(map, player);
		this.amountOfStones=countPlayerStones(map);
		stonesOnField=0;
		for(int s : amountOfStones)
			stonesOnField+=s;
		weights=new int[]{15,20,15,20,30};
		this.playableFields=StaticInfos.amPlayableFields;
	}
	
	@Override
	public int evaluate(int player, Map map) {
		initiate(map,player);
		double l=0;
		double m=0;
		double q=0;
		if(moves.getLength()!=0) {
			m=(double)mobility()*weights[0];
			q=(double)qualityOfMoves()*weights[2];
		}
		else
			l=gameWon();
		
		double e=(double)enemyMobility()*weights[1];
		double sR=(double)stonesRatio()*weights[3];
		//double sSV=(double)stonesStabilityAndValue()*weights[4];
		double erg=m+e+q+sR+l;
	//	System.out.println("m:"+m+" e:"+e+" q:"+q+" sR:"+sR+" sSV:";
		return (int)Math.round(erg)+amountOfStones[playerToEvalFor-1];
	}
	
//rating from 0-10 depending on how good own mobility on current Map is
	private int mobility() {
		double amMPossMRatio;

		if(map.getOverrideAmOf(playerToEvalFor)>0) {
			amMPossMRatio=(((double)moves.getLength())/playableFields);
			//border=((double)playableFields/10);
		}
		else {
			amMPossMRatio=((double)moves.getLength()/(playableFields-stonesOnField));
			//border=((double)(playableFields-stonesOnField)/10);
		}
		
		for(int i=1;i<=10;i++) {
			if(amMPossMRatio>=(i-1)*0.2 && amMPossMRatio<=i*0.2)
				return i;
		}
		return 0;		
	}

//rating from 0-10 depending on how bad average resulting enemy mobility with possible moves on that map is
	private int enemyMobility() {
		double rating=0;
		while(moves.hasNext()) {//look at each adjacent field and check if a move could made there
			Move m =moves.next();
			for(int dir=0;dir<8;dir++) {
				int[] adjField=goIntoDirection(m.getX(),m.getY(),dir);//get adjacent field
				if(!StaticInfos.outOfMap(adjField[0],adjField[1],map)){
					if(map.getChar(adjField[0],adjField[1])=='0')
						rating++;
					if(map.getChar(adjField[0],adjField[1])=='b')//making that move would drasticly increase chance of enemy getting b/o stone
						return 0;
				}
			}
		}
		if(moves.getLength()==0)
			return 0;
		
		rating/=moves.getLength();
		if(rating>7 && rating<=8)
			return 0;
		for(int a=1;a<=8;a++) {
			if(rating>=(a-1) && rating<=a)
				return (int)(1.25*a);
		}
		
		return -24;
	}
	
//rating from 0-10 depending on the quality of possible moves
	private int qualityOfMoves() {
		int sumOfQuality=0;
		while(moves.hasNext()) {
			Move m =moves.next();
			int tmp=StaticInfos.getTilePreRating(m.getX(), m.getY());
			if(tmp==5)
				sumOfQuality+=3*tmp;
			else
				sumOfQuality+=tmp;
		}
		
		if(sumOfQuality<0)
			return 0;
		if(sumOfQuality==0)
			return 1;
		
		double amOfMToMaxVRatio=(double)(moves.getLength()*5)/7;
		if(sumOfQuality>amOfMToMaxVRatio)
			return 10;
		for(int i=2;i<=9;i++) {
			if(sumOfQuality>=(i-1)*amOfMToMaxVRatio && sumOfQuality<=i*amOfMToMaxVRatio )
				return i;
		}
		
		return 24;//should never occur
		
	}
	
//rating from 0-10 depending on a ownStones/leaderStones ratio on field 
	private int stonesRatio() {
		int ownStones = amountOfStones[playerToEvalFor-1];
		int leader=getLeader(amountOfStones);
		
		int enemyStones=0;
		for(int s=0;s<map.getPlayerCount();s++) {
			if((s+1)!=playerToEvalFor)
				enemyStones+=amountOfStones[s];
		}	
		if(ownStones==0)//well that sucks
			return 0;
		
		double playerLeaderRatio=0;
		int factor=0;
		if(leader!=playerToEvalFor){//player isnt leader, rate gap between player and leader
			factor=1;
			playerLeaderRatio=(double)ownStones/amountOfStones[leader-1];
			
		}
		else if(leader==playerToEvalFor) {//player is leader, rate the gap between the rest+a bonus for being the leader ofc:)
			factor=2;
			playerLeaderRatio=(double)ownStones / (stonesOnField-ownStones) ;
		}
		
		for(int i=1;i<=5;i++) {
			if((0.2*(i-1))<=playerLeaderRatio && (0.2*i)>=playerLeaderRatio)
				return factor*i;
		}
		
		return -24;
	}
	
//rating from 0-20 depending on the ownStones/enemyStones "value" ratio on field+ownstableStones/enemyWithHighestAmountOfStableStones ratio on field(cant consider overwrite stones here)
//fused stability and value checking due to checking the complete map only once
	private int stonesStabilityAndValue() {
		int[] values=new int[map.getPlayerCount()];
		int[] amOfStableStones=new int[map.getPlayerCount()];
		
		for(int x=0;x<map.getHeight();x++) {//calculate the values of the stones
			for(int y=0;y<map.getWidth();y++){
				char fieldValue=map.getChar(x,y);
				if('1'>=fieldValue&&'8'<=fieldValue){
					values[fieldValue-48]+=StaticInfos.getTilePreRating(x, y);//get prerating
					
					int[] newDir;
					int tmp=0;
					for(int d=0;d<8;d++) {//and check if its a stable stone
						newDir=goIntoDirection(x,y,d);
						char tmpFieldValue=map.getChar(newDir[0],newDir[1]);
//TODO advance it and directly calculate the value of an adjacent b/c/i field!
						if(tmpFieldValue=='0'||tmpFieldValue=='b'||tmpFieldValue=='c'||tmpFieldValue=='i')
							tmp++;
					}
					if(tmp==0)
						amOfStableStones[fieldValue-48]++;
				}
			}
		}
		
		int playerValue=values[playerToEvalFor-1];//calculate the rating for stone values
		int stoneValueRating=0;
		double enemiesValue=0;
		for(int i=0;i<map.getPlayerCount();i++) {
			if(i+1!=playerToEvalFor)
				enemiesValue+=values[i];
		}
		enemiesValue/=(map.getPlayerCount()-1);
		double playerEnemyRatio=((double)playerValue/enemiesValue);
		for(int v=1;v<=10;v++) {
			if((v-1)*0.2<=playerEnemyRatio && v*0.2>=playerEnemyRatio){
				stoneValueRating=v;
				break;
			}
		}
		int stableStoneRating=0;
		int amOfPlayerStableStones=amOfStableStones[playerToEvalFor-1];//calculate the rating for stable stones
		double amEnemytStableStones=0;
		for(int i=0;i<map.getPlayerCount();i++){
			if((i+1)!=playerToEvalFor)
				amEnemytStableStones=amOfStableStones[i];	
		}
		double pEnStableStonesRatio=0;
		if(amEnemytStableStones==0)//special case, instead of player/enemy ratio calculate player/((dim.x*dim.y)/4) ratio
			pEnStableStonesRatio=(double)amOfPlayerStableStones / ( (map.getHeight()*map.getWidth()) / 4);
		else
			pEnStableStonesRatio=(double)amOfPlayerStableStones/amEnemytStableStones;
		
		if(pEnStableStonesRatio<=1) {
			for(int v=0;v<=8;v++) {
				if((v-1)*0.125<=pEnStableStonesRatio && v*0.125>=pEnStableStonesRatio) {
					stableStoneRating=v;
					break;
				}
			}
		}
		else {//more stable stones then all enemies together would be awesome
			if(1<pEnStableStonesRatio && 1.5>=pEnStableStonesRatio)
				stableStoneRating=9;
			else
				stableStoneRating=10;
		}
				
		return stoneValueRating+stableStoneRating;
	}
	
	/**
     * Useful method which calculates the next tile in a defined direction
     * @param x the x coordinate
     * @param y the y coordinate
     * @param direction the direction to go
     * @return and int[] array with length==2. contains the new tile 
     */
    public static int[] goIntoDirection(final int x,final int y,final int direction)  {
		int[] erg=new int[2];//new x,y coordinate
		if(direction==0){
			erg[0]=x;
			erg[1]=y-1;
			return erg;
		}
		if(direction==7){
			erg[0]=x-1;
			erg[1]=y-1;
			return erg;
		}
		if(direction==1){
			erg[0]=x+1;
			erg[1]=y-1;
			return erg;
		}
		if(direction==6){
			erg[0]=x-1;
			erg[1]=y;
			return erg;
		}
		if(direction==2){
			erg[0]=x+1;
			erg[1]=y;
			return erg;
		}
		if(direction==5){
			erg[0]=x-1;
			erg[1]=y+1;
			return erg;
		}
		if(direction==3){
			erg[0]=x+1;
			erg[1]=y+1;
			return erg;
		}
		if(direction==4){
			erg[0]=x;
			erg[1]=y+1;
			return erg;
		}
		return null;//throw new Exception("not a valid direction!");	
	}
    
    public static boolean outOfMap(final int x,final int y, Map map) {
 		//System.out.println("oomCheck: x "+x+"y "+y);
 		return (x<0)||(y<0)||(x>(map.getWidth()-1))||(y>(map.getHeight()-1));
    }
    
    /**
     * checks if a coordinate is still inside the map
     * @param x the x coordinate
     * @param y the y coordinate
     * @param copyOfMap the map
     * @return true if out of map, false if not
     */
    public static int[] countPlayerStones(Map m) {
		int[] amountOfStones=new int[m.getPlayerCount()];
		final int rows=m.getHeight(),columns=m.getWidth();

		for(int i=0;i<columns;i++) {
			for(int j=0;j<rows;j++) {
				final char fieldValue=m.getChar(i, j);
				if(fieldValue<='8'&&fieldValue>='1') {
					amountOfStones[(char)(fieldValue-48-1)]+=1;
				}
			}
		}
		return amountOfStones;
	}
    
    /**
     * calculates the actual leader of a map, depending on the amount of stones a player has
     * @param amountOfStones an int[] array with the amount of stones each player has on the field
     * @return the leader
     */
    public static int getLeader(final int[] amountOfStones) {
		final int l=amountOfStones.length;
		int highestAmount=0,player=1;
		
		for(int i=0;i<l;i++){
			if(highestAmount<=amountOfStones[i]) {
				highestAmount=amountOfStones[i];
				player=i+1;
			}
		}
		return player;
	}
   
    private int gameWon() {
    	int amOfP=0;
    	for(int i=0;i<this.map.getPlayerCount();i++) {
    		moves=MoveGeneratorFactory.getFrom(map, (i+1));
    		if(moves.getLength()!=0)
    			amOfP++;
    	}
    	if(amOfP==0) {
    		int l=getLeader(amountOfStones);
    		if(l==this.playerToEvalFor)
    			return 10000;
    		else
    			return -10000;
    	}
    	return 0;
    	}
	
    public static void main(String[] args) {
    	String s="./maps/phase1/Map6_endgame.map";
    	Map m=MapFactory.createFromFile(s);
		ComplexEvaluation eV=new ComplexEvaluation();
		long end=0;
		int amOfTests=100000;
		int test=0;
		for(int a=0;a<amOfTests;a++){
			long start=System.nanoTime();
			test=eV.evaluate(1,m);
			end+=System.nanoTime()-start;
		}
		System.out.println(test);
		System.out.println(end/amOfTests);
		//System.out.println((ends/1000000000)/1.9);
	}

	

}
