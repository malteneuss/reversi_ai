package ai.evaluation.experiment;

import factories.MapFactory;
import map.Map;
import map.StaticInfos;
import ai.evaluation.Evaluation;

public class NewEvaluation implements Evaluation{
	private int[] stoneValues;
	private int[] amOfPlayerTiles;
	private int amOfTiles;
	private int leader;
	
	public NewEvaluation() {}
	@Override
	public int evaluate(int player, Map map) {
		stoneValues=new int[map.getPlayerCount()];
		amOfPlayerTiles=new int[map.getPlayerCount()];
		double tR=rateTiles(player,map);
		double rAmOfT=rateAmountOfTiles(player,map);
		if(((double)amOfTiles/StaticInfos.amPlayableFields)>=0.8)
			rAmOfT*=20;
			
		return (int)(tR+rAmOfT);
	}

	private double rateTiles(int player, Map map) {
		for(int x=0;x<map.getWidth();x++) {
			for(int y=0;y<map.getHeight();y++) {
				final char fieldValue=map.getChar(x, y);
				if(fieldValue>='1'&&fieldValue<='8'){
					stoneValues[(int)(fieldValue-49)]+=StaticInfos.getTilePreRating(x,y);
					amOfPlayerTiles[(int)(fieldValue-49)]++;
				}
			}
		}
		double ownRating=0;
		double enemyRating=0;
		int amLeaderTemp=-1;
		for(int p=1;p<=map.getPlayerCount();p++){
			amOfTiles+=amOfPlayerTiles[p-1];
			if(amLeaderTemp<amOfPlayerTiles[p-1]){
				amLeaderTemp=amOfPlayerTiles[p-1];
				leader=p;
			}
			if(p==player)
				ownRating=stoneValues[p-1];
			else
				enemyRating=stoneValues[p-1];
		}
		enemyRating/=(map.getPlayerCount()-1);
		if(enemyRating!=0)
			return 100*(ownRating/enemyRating);
		else
			return 100*ownRating;
	}
	private double rateAmountOfTiles(int player, Map map) {
		double amOfOwnTiles=amOfPlayerTiles[player-1];
		double averageAmOfEnemyTiles=0;
		for(int p=1;p<=map.getPlayerCount();p++) {
			if(p==player)
				amOfOwnTiles=amOfPlayerTiles[p-1];
			else
				averageAmOfEnemyTiles+=amOfPlayerTiles[p-1];
		}
		if(averageAmOfEnemyTiles==0)
			return amOfOwnTiles;
		else
			return 100*(amOfOwnTiles/averageAmOfEnemyTiles);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s="./maps/phase1/Map6_endgame.map";
		Map m = MapFactory.createFromFile(s);
		NewEvaluation nE=new NewEvaluation();
		long end=0;
		int amOfTests=100000;
		int test=0;
		for(int a=0;a<amOfTests;a++){
			long start=System.nanoTime();
			test=nE.evaluate(2,m);
			end+=System.nanoTime()-start;
		}
		System.out.println(test);
		System.out.println(end/amOfTests);
		//System.out.println((ends/1000000000)/1.9);
	}
}
