package ai.evaluation.experiment;

import ai.evaluation.Evaluation;
import factories.MapFactory;
import map.Map;
import map.StaticInfos;


public class FusedEvaluation implements Evaluation{
	private int[] amountOfStones;
	private int[] wallsRating;
	private int[] fieldRating;
	private int amOfTiles=0;
	

	@Override
	public int evaluate(int player,Map map) {
			this.amountOfStones=new int[map.getPlayerCount()];
			this.wallsRating=new int[map.getPlayerCount()];
			this.fieldRating=new int[map.getPlayerCount()];
			double wallRating=getWallRating(map,player);
			double amOfTilesRating=rateAmountOfTiles(map, player);
			double tileValueRating=rateTileValues(map, player);
			if(((double)amOfTiles/StaticInfos.amPlayableFields)>=0.8)
				amOfTilesRating*=20;
			
			return (int)(wallRating+amOfTilesRating+tileValueRating);
	}
	
	public double rateAmountOfTiles(Map map,int player) {
		double amOfOwnTiles=amountOfStones[player-1];
		double averageAmOfEnemyTiles=0;
		for(int p=1;p<=map.getPlayerCount();p++) {
			if(p==player)
				amOfOwnTiles=amountOfStones[p-1];
			else
				averageAmOfEnemyTiles+=amountOfStones[p-1];
		}
		if(averageAmOfEnemyTiles==0)
			return amOfOwnTiles;
		else
			return 100*(amOfOwnTiles/averageAmOfEnemyTiles);
	}
	public double rateTileValues(Map map,int player) {
		double ownRating=0;
		double enemyRating=0;
		int amLeaderTemp=-1;
		int leader=0;
		for(int p=1;p<=map.getPlayerCount();p++){
			amOfTiles+=amountOfStones[p-1];
			if(amLeaderTemp<amountOfStones[p-1]){
				amLeaderTemp=amountOfStones[p-1];
				leader=p;
			}
			if(p==player)
				ownRating=fieldRating[p-1];
			else
				enemyRating+=fieldRating[p-1];
		}
		enemyRating/=(map.getPlayerCount()-1);
		if(leader==player)
			ownRating+=1000;
		if(enemyRating!=0)
			return 100*((double)ownRating/enemyRating);
		else
			return 100*ownRating;
	}
	public double getWallRating(Map map,int player) {
		apprWalls(map, player);
		double ownRating=0;
		double enemyRating=0;
		for(int p=0;p<map.getPlayerCount();p++) {
			if(p+1==player)
				ownRating=wallsRating[p];
			else
				enemyRating+=wallsRating[p];
		}
		if(enemyRating!=0)
			return 100*((double)ownRating/enemyRating);
		else
			return 100*ownRating;
		
	}
	private void apprWalls(Map map, int player) {
		this.amountOfStones=new int[map.getPlayerCount()];
		this.wallsRating=new int[map.getPlayerCount()];
		this.fieldRating=new int[map.getPlayerCount()];
		//check dir(0,4)
		char startWall='n',endWall='n';
		int startGap=0,endGap=0;
		for(int x=0;x<map.getWidth();x++) {//check dir(0,4)
			for(int y=0;y<map.getHeight();y++) {
				final char fieldVal=map.getChar(x, y);
				
				if((fieldVal<='8'&&fieldVal>='1')||fieldVal=='x') {//a possible wall detected
					if(startWall=='n')
						startWall=fieldVal;
					if(fieldVal!='x'){
						amountOfStones[fieldVal-49]++;//counting amount of playerstones once
						fieldRating[fieldVal-49]+=StaticInfos.getTilePreRating(x, y);//and determine the value of that field once
					}
					if(endWall!='n') {
						rateWall(startWall,startGap,endWall,endGap);
						startWall='n';
						endWall='n';
						startGap=0;
						endGap=0;
						continue;
					}
				}
				else if(!((fieldVal<='8'&&fieldVal>='1')||fieldVal=='x')) {//free field
					if(startWall=='n')
						startGap++;
					else {
						if(endWall=='n')
							endWall=map.getChar(x, y-1);
						endGap++;
					}
				}
				else if(fieldVal=='-'||y==map.getWidth()-1) { //hole or end of map, in both cases a possible wall ended, gets rated and a new wall is searched for
					if(!(startWall=='n'&&endWall=='n'))//special case, only 1 stone is in that dir. not a wall and therefor not calculated
						rateWall(startWall,startGap,endWall,endGap);
					
					startWall='n';
					endWall='n';
					startGap=0;
					endGap=0;
				}
				
			}
			startWall='n';
			endWall='n';
			startGap=0;
			endGap=0;
		}
		for(int y=0;y<map.getHeight();y++) {//check(2,6)
			for(int x=0;x<map.getWidth();x++) {
				final char fieldVal=map.getChar(x, y);
				
				if((fieldVal<='8'&&fieldVal>='1')||fieldVal=='x') {//a possible wall detected
					if(startWall=='n')
						startWall=fieldVal;
				}
				else if(!((fieldVal<='8'&&fieldVal>='1')||fieldVal=='x')) {//free field
					if(startWall=='n')
						startGap++;
					else {
						if(endWall=='n')
							endWall=map.getChar(x-1, y);
						
						endGap++;
					}
				}
				else if(fieldVal=='-'||y==map.getWidth()-1) { //hole or end of map, in both cases a possible wall ended, gets rated and a new wall is searched for
					if(!(startWall==endWall))//special case, only 1 stone is in that dir. not a wall and therefor not calculated
						rateWall(startWall,startGap,endWall,endGap);
					
					startWall='n';
					endWall='n';
					startGap=0;
					endGap=0;
				}
			}
			startWall='n';
			endWall='n';
			startGap=0;
			endGap=0;
		}
	}
	private void rateWall(char startWall, int startGap, char endWall, int endGap) {
		if(endWall!='x') {
			if(startGap%2==0)//even gap isnt good, because most likely you wont conquer the edge/corner
				this.wallsRating[(endWall-49)]--;
			else//odd gap is good
				this.wallsRating[(endWall-49)]++;
		}
		if(startWall!='x') {
			if(endGap%2==0)//even gap isnt good, because most likely you wont conquer the edge/corner
				this.wallsRating[(startWall-49)]--;
			else//odd gap is good
				this.wallsRating[(startWall-49)]++;
		}
	}

	public static void main(String[] args) {
		String s="./maps/phase1/Map6_endgame.map";
		Map m = MapFactory.createFromFile(s);
		FusedEvaluation ev=new FusedEvaluation();
		long end=0;
		int amOfTests=100000;
		int test=0;
		for(int a=0;a<amOfTests;a++){
			long start=System.nanoTime();
                    test = ev.evaluate(3, m);
			end+=System.nanoTime()-start;
		}
		System.out.println(end/amOfTests);
		System.out.println(test);

	}

}
