package ai.evaluation;

import map.Map;

public class SimpleEvaluation implements Evaluation {

    private static final int STONE_WEIGHT = 1;
    private static final int OVERRIDE_WEIGHT = 50;
    private int[] pointsOf;

    @Override
    public int evaluate(int playerNumber, Map map) {
        pointsOf = new int[map.getPlayerCount()];
        rateStones(map);
        rateBombsAndOverrides(map);
        //percentage of Points
        //int rating = pointsOf[playerNumber - 1] * 100 / totalPoints();

        //return rating;
        return pointsOf[playerNumber - 1];
    }

    private void rateStones(Map map) throws RuntimeException {
        //rate/count stones
        for (int y = 0, height = map.getHeight(); y < height; y++) {
            for (int x = 0, width = map.getWidth(); x < width; x++) {
                int player = map.getChar(x, y) - 48;
                if (player >= 1 && player <= map.getPlayerCount()) {
                        this.pointsOf[player - 1] += STONE_WEIGHT;
                    }
                }
            }
    }

    private void rateBombsAndOverrides(Map map) {
        int blastStrength = map.getBombStrength() * map.getBombStrength();
        //rate/count bombs and overrides
        for (int player = 1; player <= map.getPlayerCount(); player++) {
            pointsOf[player - 1] += map.getBombCountOf(player) * blastStrength;
            pointsOf[player - 1] += map.getOverrideCountOf(player) * OVERRIDE_WEIGHT;
        }
    }

    private int totalPoints() {
        return sumOf(pointsOf);
    }

    private static int sumOf(int[] rating) {
        int sum = 0;
        for (int i = 0; i < rating.length; i++) {
            sum += rating[i];
        }
        return sum;
    }
}
