package ai.evaluation;

import map.Map;

public class FastSimpleEvaluation implements Evaluation {

    private static final int STONE_WEIGHT = 1;
    private static final int OVERRIDE_WEIGHT = 50;
    private int pointsOf;

    @Override
    public int evaluate(int playerNumber, Map map) {
        for (int y = 0, height = map.getHeight(); y < height; y++) {
            for (int x = 0, width = map.getWidth(); x < width; x++) {
                int player = map.getChar(x, y) - Map.PLAYER_TO_CHAR_OFFSET;
                if (player == playerNumber) {
                    this.pointsOf += 1;
                }
            }
        }
        pointsOf += map.getOverrideCountOf(playerNumber) * OVERRIDE_WEIGHT;

        return pointsOf;
    }


}
