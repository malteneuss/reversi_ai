package ai.evaluation;

import map.Map;

public interface Evaluation {

    public int evaluate(int player, Map map);
}
