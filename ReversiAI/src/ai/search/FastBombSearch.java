package ai.search;

import ai.evaluation.SimpleEvaluation;
import factories.MapFactory;
import factories.MoveFactory;
import java.awt.image.TileObserver;
import java.util.List;
import java.util.Set;
import map.Map;
import move.BombMove;
import move.InvalidMove;
import move.Move;
import move.helper.Bomber;
import move.helper.TilePosition;

public class FastBombSearch extends CountingSearch {

    private Map map;
    private int player;

    public FastBombSearch(Map map, int player) {
        this.map = map;
        this.player = player;
    }

    @Override
    public Move getMove(int depth, long timeLeftInMs) {
        int attackedPlayer = determineAttackedPlayer();

        Move bestMove = null;
        int bestDifference = Integer.MIN_VALUE;
        for (int y = 0; y < map.getHeight(); y++) {
            for (int x = 0; x < map.getWidth(); x++) {
                if (!map.isHoleAt(x, y)) {
                    int difference = getDifferenceOfBombing(x, y, attackedPlayer);
                    if (difference > bestDifference) {
                            bestMove = MoveFactory.getBombMoveFrom(map, player, x, y);
                    }
                }
            }
        }


        return bestMove;
    }

    @Override
    public String getName() {
        return "FastBombSearch";
    }

    private int determineAttackedPlayer() {
        final int playerCount = map.getPlayerCount();
        int[] stonesOf = getStoneCounts();
        int ourStones = stonesOf[player - 1];
        int playerAbove = -1;
        int stonesOfAbove = Integer.MAX_VALUE;
        int playerBelow = -1;
        int stonesOfBelow = Integer.MIN_VALUE;
        for (int i = 0; i < playerCount; i++) {
            if (i == player) {
                continue;
            }
            int currentStones = stonesOf[i];
            if (currentStones >= ourStones) {
                if (currentStones < stonesOfAbove) {
                    playerAbove = i;
                    stonesOfAbove = currentStones;
                }
            }

            if (currentStones <= ourStones) {
                if (currentStones > stonesOfBelow) {
                    playerAbove = i;
                    stonesOfBelow = currentStones;
                }
            }
        }
        int attackedPlayer = (playerAbove == -1) ? playerBelow : playerAbove;
        return attackedPlayer;
    }

    private int[] getStoneCounts() {
        int[] stonesOf = new int[map.getPlayerCount()];
        for (int y = 0; y < map.getHeight(); y++) {
            for (int x = 0; x < map.getWidth(); x++) {
                if (map.isAnyPlayerAt(x, y)) {
                    int playerAtXY = map.getChar(x, y) - Map.PLAYER_TO_CHAR_OFFSET - 1;
                    stonesOf[playerAtXY] += 1;
                }
            }
        }
        return stonesOf;
    }

    private int getDifferenceOfBombing(int x, int y, int attackedPlayer) {
        Set<TilePosition> positions = Bomber.collectDetonatedPosition(map, x, y, map.getBombStrength());
        int destroyedOfEnemy = 0;
        int destroyedByUs = 0;
        for (TilePosition t : positions) {
            int temp = t.getChar() - Map.PLAYER_TO_CHAR_OFFSET - 1;
            if (temp == player - 1) {
                destroyedByUs++;

            } else if (temp == attackedPlayer) {
                destroyedOfEnemy++;
            }

        }
        int difference = (destroyedOfEnemy - destroyedByUs);
        return difference;
    }

    public static void main(String[] args) {
        Map map = MapFactory.createFromFile("./maps/bombTest1.map");
        int player = 1;
        System.out.println(map);
        SearchStrategy search = new FastBombSearch(map, player);
        Move move = search.getMove(14, Long.MAX_VALUE);
        System.out.println(move + " ");
    }
}
