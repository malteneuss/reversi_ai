
package ai.search;

import move.Move;

public interface SearchStrategy {

    public Move getMove(int depth, long timeLeftInMs);

    public long getNodeCount();
    public long getEvalCount();

    public String getName();
}
