package ai.search;

import factories.MoveFactory;
import map.Map;
import move.Move;

public class StupidBombSearch extends CountingSearch {

    private Map map;
    private int player;

    public StupidBombSearch(Map map, int player) {
        this.map = map;
        this.player = player;
    }

    @Override
    public Move getMove(int depth, long timeLeftInMs) {
        for (int y = 0; y < map.getHeight(); y++) {
            for (int x = 0; x < map.getWidth(); x++) {
                if (map.isBombableAt(x, y)) {
                    return MoveFactory.getBombMoveFrom(map, player, x, y);
                }
            }
        }
        return null;//should never happen
    }

    @Override
    public String getName() {
        return "StupidBombMove";
    }
}
