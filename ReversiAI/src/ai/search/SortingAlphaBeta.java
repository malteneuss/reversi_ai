package ai.search;

import ai.evaluation.Evaluation;
import map.Map;
import movegenerator.MoveGenerator;

public class SortingAlphaBeta extends AlphaBeta {
//TODO killermove heuristic missing
    public SortingAlphaBeta(Map map, int player, Evaluation evaluator) {
        super(map, player, evaluator);

    }

    @Override
    protected MoveGenerator getMoveGenerator(Map local, int currentPlayer) {
        throw new UnsupportedOperationException("No ready yet");
//        if (currentPlayer == super.player) {
//            return super.getMoveGenerator(local, currentPlayer);
//        } else {
//            return super.getMoveGenerator(local, currentPlayer);
//        }
    }

    @Override
    public String getName() {
        return "SortingAlphaBeta";
    }
}
