package ai.search;

import ai.evaluation.Evaluation;
import ai.evaluation.SimpleEvaluation;

import ai.search.aspirationWindows.ItDeepeningAspWindows;
import core.StaticInfo;
import factories.MapFactory;
import factories.MoveFactory;
import factories.MoveGeneratorFactory;
import map.Map;
import map.StaticInfos;
import move.Move;
import movegenerator.MoveGenerator;

public class AlphaBeta extends CountingSearch {
	int a=0,b=0;
    public AlphaBeta(Map map, int player, Evaluation evaluator) {
        super.map = map;
        super.player = player;
        super.evaluator = evaluator;
    }

    @Override
    public Move getMove(int depth, long timeLeftInMs) {
        int initialAlpha = Integer.MIN_VALUE;
        int initialBeta = Integer.MAX_VALUE;
        return getMove(depth, timeLeftInMs, initialAlpha, initialBeta);
    }

    /**
     * Does exactly what getMove( depth, timeLeftInMs) does, but initial Alpha
     * and Beta can be set.
     */
    public Move getMove(int depth, long timeLeftInMs, int alpha, int beta) {
        super.restartClock(timeLeftInMs);
        super.resetEvalAndNodeCounters();
        super.nodeCount++;
        //
        super.bestMove = MoveFactory.getInvalidMove(Integer.MIN_VALUE);//dummy Move

        MoveGenerator generator = getMoveGenerator(map, player);
        while (generator.hasNext()) {
            if (super.isTimeUp()) {
                return bestMove;
            }
            Move move = generator.next();
            map = move.getMapAfterMove();

            int nextPlayer = getNextPly(player);
            int rating = this.getMinValue(depth - 1, map, nextPlayer, 0, alpha, beta);
            
            if (rating >= super.bestMove.getRating()) {
            	super.bestMove = move;
                super.bestMove.setRating(rating);
                a=alpha;
                b=beta;
            }

            map = move.getMapAfterUndoMove();//important to undo the Move
            alpha = Math.max(alpha, rating);
        }
        return super.bestMove;
    }

    public int getMinValue(int depth, Map local, int currentPlayer, int loop, int alpha, int beta) {
        super.nodeCount++;
        if (depth <= 0) {
            super.evaluationCount++;
            return evaluator.evaluate(player, local);
        }

        MoveGenerator generator = getMoveGenerator(local, currentPlayer);
        if (!generator.hasNext()) {//eaisier than check for next player and loop prevention
            loop++;
            if (loop < map.getPlayerCount()) {
                int nextPlayer = getNextPly(currentPlayer);
                if (nextPlayer != player) {
                    return getMinValue(depth - 1, local, nextPlayer, loop, alpha, beta);
                } else {
                    return getMaxValue(depth - 1, local, loop, alpha, beta);
                }
            } else {//endofphase1
                super.evaluationCount++;
                return evaluator.evaluate(player, local);
            }
        }

        int lowestRating = Integer.MAX_VALUE;
        while (generator.hasNext()) {
            if (super.isTimeUp()) {
                return Integer.MIN_VALUE;
            }
            Move move = generator.next();
            local = move.getMapAfterMove();

            int nextPlayer = getNextPly(currentPlayer);
            if (nextPlayer != player) {
                lowestRating = Math.min(lowestRating,
                        getMinValue(depth - 1, local, nextPlayer, 0, alpha, beta));
            } else {
                lowestRating = Math.min(lowestRating,
                        getMaxValue(depth - 1, local, 0, alpha, beta));
            }

            local = move.getMapAfterUndoMove();//important to undo the Move

            if (lowestRating <= alpha) {
                return lowestRating;//cut off
            }
            beta = Math.min(beta, lowestRating);
        }
        return lowestRating;
    }

    public int getMaxValue(int depth, Map local, int loop, int alpha, int beta) {
        super.nodeCount++;
        if (depth <= 0) {
            super.evaluationCount++;
            return evaluator.evaluate(player, local);
        }

        MoveGenerator generator = getMoveGenerator(local, player);
        if (!generator.hasNext()) {//eaisier than check for next player and loop prevention
            loop++;
            if (loop < map.getPlayerCount()) {
                int nextPlayer = getNextPly(player);
                return getMinValue(depth - 1, local, nextPlayer, loop, alpha, beta);
            } else {//endofphase1
                super.evaluationCount++;
                return evaluator.evaluate(player, local);
            }
        }

        int highestRating = Integer.MIN_VALUE;
        while (generator.hasNext()) {
            if (super.isTimeUp()) {
                return highestRating;
            }
            Move move = generator.next();
            local = move.getMapAfterMove();

            int nextPlayer = getNextPly(player);
            highestRating = Math.max(highestRating,
                    this.getMinValue(depth - 1, local, nextPlayer, 0, alpha, beta));

            local = move.getMapAfterUndoMove();//important to undo the Move

            if (highestRating >= beta) {
                return highestRating;//(cut off)
            }
            alpha = Math.max(highestRating, alpha);
        }
        return highestRating;
    }

    public int getAlpha() {
    	return this.a;
    }
    public int getBeta() {
    	return this.b;
    }
    private int getNextPly(int player) {
        //return ((player) % map.getPlayerCount()) + 1;
        return StaticInfo.playerAfter[player];
    }

    @Override
    public String getName() {
        return "AlphaBeta";
    }

    protected MoveGenerator getMoveGenerator(Map local, int currentPlayer) {
        MoveGenerator generator = MoveGeneratorFactory.getFrom(local, currentPlayer);
        return generator;
    }

    public static void main(String[] args) {
        Map map = MapFactory.createFromFile("./maps/phase1/2TurnInARow.map");
        int player = 1;

        System.out.println(map);
        SearchStrategy search = new AlphaBeta(map, player, new SimpleEvaluation());
        Move move = search.getMove(14, Long.MAX_VALUE);
        System.out.println(move + " " + search.getNodeCount());
    }
}
