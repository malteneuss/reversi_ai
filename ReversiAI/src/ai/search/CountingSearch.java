package ai.search;

import ai.evaluation.Evaluation;

import ai.search.SearchStrategy;
import map.Map;

import move.Move;

public abstract class CountingSearch implements SearchStrategy {

    protected Map map;
    protected int player;
    protected Evaluation evaluator;
    protected Move bestMove;

    protected int nodeCount = 0;
    protected int evaluationCount = 0;
    private long startTime;
    private long timeLeftInMs;

    @Override
    public abstract Move getMove(int depth, long timeLeftInMs);

    @Override
    public long getNodeCount() {
        return nodeCount;
    }

    @Override
    public long getEvalCount() {
        return evaluationCount;
    }
    /**
     * Sets them to 0
     */
    protected void resetEvalAndNodeCounters() {
        this.nodeCount = 0;
        this.evaluationCount = 0;
    }

    /*
     * Restarts measuring the time
     */
    protected void restartClock(long timeLeftInMs) {
        startTime = System.currentTimeMillis();
        this.timeLeftInMs = timeLeftInMs;
    }
    protected boolean isTimeUp() {
        return System.currentTimeMillis() - startTime > timeLeftInMs;
    }
}
