package ai.search;

import java.util.ArrayList;
import java.util.List;
import factories.MapFactory;
import factories.MoveFactory;

import map.Map;
import map.StaticInfos;
import move.Move;
import move.helper.Bomber;


public class BestBombMove extends CountingSearch {
	private Move bestMove;
	public int factorRatingMax=0;
	public int factorRatingMin=0;
	private int minPlayer=-1;//we try to minimize the gap between us and minPlayer(only if we arent last place, else its -1)
	private int maxPlayer=-2;//we try to maximize the gap between us and maxPlayer(only if we arent leader, else its -2)

	private int[] amountOfPlayerStones;
	
	private int[] chosenQuadrant;
	
	private List<Move> firstQuadrant;//the generated move sorted(for further details check preSelectBombMoves() )
	private List<Move> secondQuadrant;
	private List<Move> rest;

	private Map actMap;
	
	public static long timer=Integer.MIN_VALUE;
	public static Move timerMove;
	
	public  String getInfos() {
		return "player to catch up with: "+minPlayer+", player to maximize gap to: "+maxPlayer;
	}
	
	public BestBombMove(Map map, int player) {
		this.actMap = map;
        this.player = player;
		this.amountOfPlayerStones=countPlayerStones(map);
		getTargets();
		this.chosenQuadrant=getMostPromisingAreas();
		
        this.bestMove=MoveFactory.getBombMoveFrom(map, player, -12, -12);

        this.firstQuadrant=new ArrayList<>();
        this.secondQuadrant=new ArrayList<>();
        this.rest=new ArrayList<>();
        preSelectBombMoves();

        this.actMap = map;
    }

//chooses the best bomb move and returns it
	@Override
	public Move getMove(int depth, long timeLeftInMs) {
		super.restartClock(timeLeftInMs);//reset everything for timing
        super.resetEvalAndNodeCounters();
        
        int rating=Integer.MIN_VALUE;//initialize rating as low as possible
		List<Move> moveContainer=firstQuadrant;
	
		for(int q=0;q<3;q++) {
			for(Move bombMove :moveContainer) {//check the best rated Quadrant and choose best move
				if (super.isTimeUp()) 
					return bestMove;
				
				Map copy=actMap.copy();//make the actual move...
				Bomber.detonate(copy, bombMove.getX(), bombMove.getY(), copy.getBombStrength());
			
				int[] newAmOfPlayerStones=countPlayerStones(copy);//Count the player stones on the recently bombed map
				int tempRating=Integer.MIN_VALUE;
				try{
				tempRating=rateMove(newAmOfPlayerStones);
				} catch(Exception e){
					e.toString();
				}

				//System.out.println("rating of "+bombMove.getX()+"|"+bombMove.getY()+" is: " +tempRating);
				bombMove.getMapAfterUndoMove();
				if(rating<=tempRating){//check if a new best move is born
					rating=tempRating;
					bestMove=bombMove;
				}
				if (super.isTimeUp())
		            return bestMove;
				
			}
		
			if (super.isTimeUp())
				return bestMove;
			if(q==1)
				moveContainer=secondQuadrant;
			else
				moveContainer=rest;
			}
		
		return bestMove;
        
		
	}

	public Move apprBestMove(int depth, long timeLeftInMs) {

		super.restartClock(timeLeftInMs);//reset everything for timing
        super.resetEvalAndNodeCounters();
        
		int rating=Integer.MIN_VALUE;//initialize rating as low as possible
		List<Move> moveContainer=firstQuadrant;
		for(int q=0;q<3;q++) {
			for(Move bombMove :moveContainer) {//check the best rated Quadrant and choose best move
				if (super.isTimeUp()) 
					return bestMove;
				int[] newAmOfPlayerStones=new int[this.actMap.getPlayerCount()];
				int[] amOfBombedStones=new int[this.actMap.getPlayerCount()];
				int leftTopBorder=0,rightTopBorder=0,leftBottomBorder=0,rightBottomBorder=0;
				if(bombMove.getX()-this.actMap.getBombStrength()>0)
					leftTopBorder=bombMove.getX()-this.actMap.getBombStrength();
				else
					leftTopBorder=0;
				if(bombMove.getX()+this.actMap.getBombStrength()<this.actMap.getWidth())
					rightTopBorder=bombMove.getX()+this.actMap.getBombStrength();
				else
					rightTopBorder=this.actMap.getWidth();
				if(bombMove.getY()-this.actMap.getBombStrength()>0)
					leftBottomBorder=bombMove.getY()-this.actMap.getBombStrength();
				else
					leftBottomBorder=0;
				if(bombMove.getY()+this.actMap.getBombStrength()<this.actMap.getHeight())
					leftBottomBorder=bombMove.getY()+this.actMap.getBombStrength();
				else
					leftBottomBorder=this.actMap.getHeight();
				
				for(int x=leftTopBorder;x<rightTopBorder;x++) {
					for(int y=leftBottomBorder;x<rightBottomBorder;y++) {
						char fieldValue=this.actMap.getChar(x, y);
						if(fieldValue>='1'&fieldValue<='8')
							amOfBombedStones[(int)(fieldValue-49)]++;
					}
				}
				for(int p=0;p<this.actMap.getPlayerCount();p++)
					newAmOfPlayerStones[p]=amountOfPlayerStones[p]-amOfBombedStones[p];
			
				int tempRating=Integer.MIN_VALUE;
				try{
				tempRating=rateMove(newAmOfPlayerStones);
				} catch(Exception e){
					e.toString();
				}

				//System.out.println("rating of "+bombMove.getX()+"|"+bombMove.getY()+" is: " +tempRating);
				bombMove.getMapAfterUndoMove();
				if(rating<=tempRating){//check if a new best move is born
					rating=tempRating;
					bestMove=bombMove;
				}
				if (super.isTimeUp())
		            return bestMove;	
			}
		
			if (super.isTimeUp())
				return bestMove;
			if(q==1)
				moveContainer=secondQuadrant;
			else
				moveContainer=rest;
			}
		
		return bestMove;
	}
//counts stones for all players	
	private static int[] countPlayerStones(Map m) {
		int[] amountOfStones=new int[m.getPlayerCount()];
		final int rows=m.getHeight(),columns=m.getWidth();

		for(int i=0;i<columns;i++) {
			for(int j=0;j<rows;j++) {
				final char fieldValue=m.getChar(i, j);
				if(fieldValue<='8'&&fieldValue>='1') {
					amountOfStones[(char)(fieldValue-48-1)]+=1;
				}
			}
		}
		return amountOfStones;
	}
	
//calculates a hierarchy on the importance of bombing the players
//case 1 - player not leader: maximize the gap of player below and minimize gap of player above
//case 2 - player is leader: maximize the gap of player below
//case 3 - player is last place: minimize the gap of player above
	private void getTargets() {	
		int ourStoneCount=this.amountOfPlayerStones[this.player-1];
		int maxCandidateCount=Integer.MIN_VALUE;
		int maxCandidate=-2;
		int minCandidateCount=Integer.MAX_VALUE;
		int minCandidate=-1;
			
		final int l=this.amountOfPlayerStones.length;
		for(int i=0;i<l;i++){
			if((i+1)==super.player)
				continue;
			if(ourStoneCount<this.amountOfPlayerStones[i]&&minCandidateCount>=this.amountOfPlayerStones[i]) {
				minCandidate=i+1;
				minCandidateCount=this.amountOfPlayerStones[i];
			}
			else if(ourStoneCount>this.amountOfPlayerStones[i]&&maxCandidateCount<=this.amountOfPlayerStones[i]) {
				maxCandidate=i+1;
				maxCandidateCount=this.amountOfPlayerStones[i];
			}
		}
		int catchUp=(2+this.actMap.getBombStrength()+1)*(2+this.actMap.getBombStrength()+1);//calculating the max amount to destroy with one bomb
		if(minCandidate>0&&(catchUp*this.actMap.getBombCountOf(this.player)>this.amountOfPlayerStones[this.player-1]-this.amountOfPlayerStones[minCandidate-1])) {//calculating if we are able to catch up
			factorRatingMax=0;
			factorRatingMin=25;
		}
		else if(minCandidate>0) {
			factorRatingMax=25;
			factorRatingMin=0;
		}
		else {
			factorRatingMax=20;
			factorRatingMin=5;
		}
		this.maxPlayer=maxCandidate;
		this.minPlayer=minCandidate;
	}
	
			
//	splits map in 9 quadrants and calculates most promising quadrant to search for the best bomb move(they are partially overlappin to avoid missed moves)
//	 q0	   q01 		 q1
//	q02	   q0123	q13
//	 q2	   q23 		 q3
//returns the border of the best quadrant - e.g. q1 got chosen: x 0-23,y 0-12 ->int[]{0,23,0,12}
	private int[] getMostPromisingAreas() {
		int[] rating=new int[9];
	
		for(int i=0;i<9;i++) {//calculate enemyStones/ownStones ratio for each quadrant
			for(int x=StaticInfos.borders[i][0];x<=StaticInfos.borders[i][1];x++) {//q1
				for(int y=StaticInfos.borders[i][2];y<=StaticInfos.borders[i][3];y++) {
					final int fieldValue=(int)(actMap.getChar(x, y)-48);
					if(fieldValue>=1&&fieldValue<=8) { 
						if(fieldValue==super.player)//bombing own stones is bad unless its a good ownStones : enemyStones ratio
							rating[i]-=20;
						else if(fieldValue==this.minPlayer)
							rating[i]+=20;
						else if(fieldValue==this.maxPlayer)
							rating[i]+=5;
						else
							rating[i]+=1;
					}
				}
			}
		}
		
		int chosenQuadrantFirst=1;
		int chosenQuadrantSecond=1;
		int tmpRatingFirst=Integer.MIN_VALUE;
		for(int i=0;i<8;i++){//get best quadrant
			if(tmpRatingFirst<=rating[i]){
				chosenQuadrantSecond=chosenQuadrantFirst;
				tmpRatingFirst=rating[i];
				chosenQuadrantFirst=i+1;
			}
		}
		
		int[] res=new int[8];
		for(int i=0;i<4;i++)
			res[i]=StaticInfos.borders[chosenQuadrantFirst][i];
		for(int i=0;i<4;i++)
			res[i+4]=StaticInfos.borders[chosenQuadrantSecond][i];
		chosenQuadrantFirst=1+1;
		return res;
	}

//Select Bomb Moves which are in the most promising quadrant
	private void preSelectBombMoves() {
		this.chosenQuadrant=getMostPromisingAreas();
			
		for(int x=0;x<actMap.getWidth();x++) {
			for(int y=0;y<actMap.getHeight();y++) {
				if(actMap.getChar(x, y)!='-') {
					if(this.chosenQuadrant[0]<=x&&this.chosenQuadrant[1]>=x&&this.chosenQuadrant[2]<=y&&this.chosenQuadrant[3]>=y)
						this.firstQuadrant.add(MoveFactory.getBombMoveFrom(actMap,player,x,y));
					else if(this.chosenQuadrant[4]<=x&&this.chosenQuadrant[5]>=x&&this.chosenQuadrant[6]<=y&&this.chosenQuadrant[7]>=y)
						this.secondQuadrant.add(MoveFactory.getBombMoveFrom(actMap,player,x,y));
					else
						this.rest.add(MoveFactory.getBombMoveFrom(actMap,player,x,y));
				}
			}
		}
	}

	private int rateMove(int[] newAmOfPlayerStones) throws Exception {
		int oldGap=0;
		int newGap=0;
		int rating=0;
		double temp=0;
		if(minPlayer!=-1) {//rate the change of the gap between us and player above
			oldGap=this.amountOfPlayerStones[minPlayer-1]-this.amountOfPlayerStones[super.player-1];
			newGap=newAmOfPlayerStones[minPlayer-1]-newAmOfPlayerStones[super.player-1];
			rating+=20*factorRatingMin*(oldGap-newGap);
		}
		if(maxPlayer!=-2) {//rate the change of the gap between us and player below
			oldGap=this.amountOfPlayerStones[super.player-1]-this.amountOfPlayerStones[maxPlayer-1];
			newGap=newAmOfPlayerStones[super.player-1]-newAmOfPlayerStones[maxPlayer-1];
			rating+=20*factorRatingMax*(newGap-oldGap);
		}
		if(minPlayer==-1&&maxPlayer==-2) {
			for(int i=0;i<this.actMap.getPlayerCount();i++) {
				if(i+1==super.player)
					continue;
				oldGap+=this.amountOfPlayerStones[super.player-1]-this.amountOfPlayerStones[i];
				newGap+=newAmOfPlayerStones[super.player-1]-newAmOfPlayerStones[i];
				temp=20*((newGap-oldGap)/(this.actMap.getPlayerCount()-1));
				rating=(int)Math.round(temp);
			}
		}
		return rating;
	}
	
	private static int getWinner(Map m) {
		int[] amountOfStones=countPlayerStones(m);
		int mostStones=0;
		int winner=0;

		for(int i=0;i<amountOfStones.length;i++) {
			if(amountOfStones[i]>mostStones) {
				mostStones=amountOfStones[i];
				winner=i+1;
			}
		}
		for(int i=0;i<amountOfStones.length;i++) {
			System.out.println("Player "+(i+1)+" has: "+amountOfStones[i]+" stones");
		}
		System.out.println("winner is: "+winner);
		return winner;
	}
	
	public static void main(String[] args) {
		String s="./maps/phase2/RoundedMap.map";
    	int playerToBombFor=3;
    	int amOfTests=50;
		Map m = MapFactory.createFromFile(s);
        //StaticInfo.gamePhase = 2;
    	m.setBombCountOf(playerToBombFor, amOfTests);
 
    	long start=0,end=0;

    	//System.out.println(m.toString());
    	System.out.println("running tests with bomb power: "+m.getBombStrength()+" throwing: "+amOfTests+" bombs");
    	BestBombMove.getWinner(m);
    	BestBombMove bM=new BestBombMove(m,playerToBombFor);
    	for (int i=0;i<amOfTests;i++) {
    		start=System.nanoTime();
    		Move move=bM.getMove(1, (long)(2000));//takes in average 3.5-4 secs without timelimit!
    		end+=(System.nanoTime()-start);
    	}
    	System.out.println(end/amOfTests);
    	//System.out.println(m.toString());
    	BestBombMove.getWinner(m);	
	}
	@Override
	public String getName() {
            return "BestBombMove";
	}
			
}
