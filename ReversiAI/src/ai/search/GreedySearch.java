 package ai.search;

import java.util.List;
import ai.evaluation.Evaluation;
import factories.MoveGeneratorFactory;
import java.util.ArrayList;
import map.Map;
import move.Move;
import movegenerator.MoveGenerator;


public class GreedySearch implements SearchStrategy {
    
    private Map map;
    private int ownPlayerNumber;

    public GreedySearch(Map map, int ownPlayerNumber, Evaluation e){
        this.map = map;
        this.ownPlayerNumber = ownPlayerNumber;
    }
    
    
    @Override
    /**
     * Rates all moves and pick the best one. The best one in this case is the
     * one that either increases the distance to the next in our ranking or 
     * decreasing the distance to the previous  in opponent in our ranking.
     * 
     */
    public Move getMove(int depth, long timeLeftInMs) {

        Move bestMove;
        List<Move> moveList = new ArrayList<>();
        MoveGenerator generator = MoveGeneratorFactory.getFrom(map, ownPlayerNumber);
        while (generator.hasNext()) {
            moveList.add(generator.next());
        }
        int moveNumber = 0;
        int lastRank = this.countStonesFor(null)[this.ownPlayerNumber*2-2];
        System.out.println(this.ownPlayerNumber+" hat Rang "+lastRank);
        int diffToNextBetterPlayer = Integer.MAX_VALUE;
        int diffToWorsePlayer = 0;
        
        for(int i = 1; i < moveList.size(); i++){
            int[] compare = countStonesFor(moveList.get(i));
            
            int nextBetterPlayer = this.getNextBetterPlayer(compare);
            int nextWorsePlayer = this.getNextWorsePlayer(compare);
            
            // is the current rank better ?
            if(lastRank > compare[2*this.ownPlayerNumber-2]){
                lastRank = compare[2*this.ownPlayerNumber-2];
                bestMove = moveList.get(moveNumber);
                bestMove.setRating(compare[2 * this.ownPlayerNumber - 1]);
            }
            

            
            if(nextBetterPlayer != 0  && (compare[2*nextBetterPlayer-1]-compare[2*this.ownPlayerNumber-1]) < diffToNextBetterPlayer){
                moveNumber = i;
                diffToNextBetterPlayer = compare[2*nextBetterPlayer-1]-compare[2*this.ownPlayerNumber-1];
            }
            if(nextWorsePlayer != 0 && compare[2*this.ownPlayerNumber-1]-compare[2*nextWorsePlayer-1] > diffToWorsePlayer){
                moveNumber = i;
                diffToWorsePlayer = compare[2*this.ownPlayerNumber-1] - compare[2*nextWorsePlayer-1];
            }

        }

        bestMove = moveList.get(moveNumber);
        bestMove.setRating(8 - lastRank);
    
        
        
        return bestMove;
    }

    @Override
    public long getNodeCount() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getEvalCount() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    /**
     * Calculates the number of stones for each player and rank will rank the
     * players. So the player with the most stones will be on position one. This
     * happens in an array. Each player got 2 array entries. The first to entries
     * of the array belong to player one. The entries look like this:
     * 
     * [player ones place][player ones number of stones]
     * [player twos place][player twos number of stones]
     * .
     * :
     * 
     * @param move
     * @return Array with ranked player like the given example
     */
    private int[] countStonesFor(Move move){
        int[] stoneNumberWithRanking = new int[this.map.getPlayerCount()*2];
        
        for(int x = 0; x < map.getWidth(); x++){
            for(int y = 0; y < map.getHeight(); y++){
                if(map.isPlayerAt(1, x, y))
                    stoneNumberWithRanking[1]++;
                else if(map.isPlayerAt(2, x, y))
                    stoneNumberWithRanking[3]++;
                else if(map.isPlayerAt(3, x, y))
                    stoneNumberWithRanking[5]++;
                else if(map.isPlayerAt(4, x, y))
                    stoneNumberWithRanking[7]++;
                else if(map.isPlayerAt(5, x, y))
                    stoneNumberWithRanking[9]++;
                else if(map.isPlayerAt(6, x, y))
                    stoneNumberWithRanking[11]++;
                else if(map.isPlayerAt(7, x, y))
                    stoneNumberWithRanking[13]++;
                else if(map.isPlayerAt(8, x, y))
                    stoneNumberWithRanking[15]++;
            }
        }        
        
        return setRanks(stoneNumberWithRanking);
        
    }
    
    /**
     * Calculates the ranks of a given array. The array must be structured like this:
     * [rank of player one][rank will be inserted here]
     * [rank of player two][rank will be inserted here]
     * .
     * .
     * .
     * @param list
     * @return the given list where ranks are inserted
     */
    private int[] setRanks(int[] list){
        
        for(int player = 1; player <= this.map.getPlayerCount(); player++){
            list[2*player-2] = player;
        }
        
        int max = 0;
        int maxPlayer = 0;
        int oldMax = Integer.MAX_VALUE;
        
        for(int rank = 1; rank <= this.map.getPlayerCount(); rank++){
            max = 0;
            for(int player = 1; player <= this.map.getPlayerCount(); player++){
                if(list[2*player-1] > max && list[2*player-1] < oldMax){
                    max = list[2*player-1];
                    maxPlayer = player;
                }
            
            }
            oldMax = max; max = 0;
            list[2*maxPlayer-2] = rank;
        }
        
        
        return list;
    }
    
    /**
     * Caluculates the next better player with the given array. The array must be structured
     * like this:
     * [rank of player one][this entry will be ignored]
     * [rank of player two][this entry will be ignored]
     * .
     * .
     * .
     * This method ignores every second entry. If the return value of this method returns 0,
     * no other player is better than the one of the AI
     * @param list
     * @return number of the next better player
     */
    private int getNextBetterPlayer(int[] list){
        
        
        int nextBetterPlayer = 0;
        int rankOfNextPlayer = list[2*this.ownPlayerNumber-2]-1;
        
        for(int player = 1; player <= this.map.getPlayerCount(); player++){
            if(rankOfNextPlayer == list[2*player-2])
                nextBetterPlayer = player;
        }
        return nextBetterPlayer;
    }
    /**
     * Caluculates the next worse player with the given array. The array must be structured
     * like this:
     * [rank of player one][this entry will be ignored]
     * [rank of player two][this entry will be ignored]
     * .
     * .
     * .
     * This method ignores every second entry. If the return value of this method returns 0,
     * no other player is worse than the one of the AI
     * @param list
     * @return number of the next worse player
     */
    private int getNextWorsePlayer(int[] list){
        int nextWorsePlayer = 0;
        int rankOfNextPlayer = list[2*this.ownPlayerNumber-2]+1;
        
        for(int player = 1; player <= this.map.getPlayerCount(); player++){
            if(rankOfNextPlayer == list[2*player-2])
                nextWorsePlayer = player;
        }
        return nextWorsePlayer;
    }

    @Override
    public String getName() {
        return "GreedySearch";
    }
 
}

