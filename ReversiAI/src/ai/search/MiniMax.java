package ai.search;

import ai.search.CountingSearch;
import ai.evaluation.Evaluation;
import ai.evaluation.SimpleEvaluation;
import factories.MapFactory;
import factories.MoveFactory;
import factories.MoveGeneratorFactory;
import map.Map;
import move.Move;
import movegenerator.MoveGenerator;

public class MiniMax extends CountingSearch {

    public MiniMax(Map map, int player, Evaluation evaluator) {
        super.map = map;
        super.player = player;
        super.evaluator = evaluator;
    }

    @Override
    public Move getMove(int depth, long timeLeftInMs) {
        super.restartClock(timeLeftInMs);
        super.resetEvalAndNodeCounters();
        super.nodeCount++;
        //
        super.bestMove = MoveFactory.getInvalidMove(Integer.MIN_VALUE);//dummy Move
        MoveGenerator generator = MoveGeneratorFactory.getFrom(map, player);
        while (generator.hasNext()) {
            if (super.isTimeUp()) {
                return bestMove;
            }
            Move move = generator.next();
            map = move.getMapAfterMove();

            int nextPlayer = getNextPly(player);
            int rating = this.getMinValue(depth - 1, map, nextPlayer, 0);

            if (rating >= super.bestMove.getRating()) {
                super.bestMove = move;
                super.bestMove.setRating(rating);
            }
            map = move.getMapAfterUndoMove();//important to undo the Move
        }
        return super.bestMove;
    }

    public int getMinValue(int depth, Map local, int currentPlayer, int loop) {
        super.nodeCount++;
        if (depth <= 0) {
            super.evaluationCount++;
            return evaluator.evaluate(player, local);
        }

        MoveGenerator generator = MoveGeneratorFactory.getFrom(local, currentPlayer);
        if (!generator.hasNext()) {//eaisier than check for next player and loop prevention
            loop++;
            if (loop < map.getPlayerCount()) {
                int nextPlayer = getNextPly(currentPlayer);
                if (nextPlayer != player) {
                    return getMinValue(depth - 1, local, nextPlayer, loop);
                } else {
                    return getMaxValue(depth - 1, local, loop);
                }
            } else {//endofphase1
                super.evaluationCount++;
                return evaluator.evaluate(player, local);
            }
        }

        int lowestRating = Integer.MAX_VALUE;
        while (generator.hasNext()) {
            if (super.isTimeUp()) {
                return Integer.MIN_VALUE;
            }
            Move move = generator.next();
            local = move.getMapAfterMove();

            int nextPlayer = getNextPly(currentPlayer);
            if (nextPlayer != player) {
                lowestRating = Math.min(lowestRating, getMinValue(depth - 1, local, nextPlayer, 0));
            } else {
                lowestRating = Math.min(lowestRating, getMaxValue(depth - 1, local, 0));
            }

            local = move.getMapAfterUndoMove();//important to undo the Move
        }
        return lowestRating;
    }

    public int getMaxValue(int depth, Map local, int loop) {
        super.nodeCount++;
        if (depth <= 0) {
            super.evaluationCount++;
            return evaluator.evaluate(player, local);
        }


        MoveGenerator generator = MoveGeneratorFactory.getFrom(local, player);
        if (!generator.hasNext()) {//eaisier than check for next player and loop prevention
            loop++;
            if (loop < map.getPlayerCount()) {
                int nextPlayer = getNextPly(player);
                return getMinValue(depth - 1, local, nextPlayer, loop);  
        } else {//endofphase1
            super.evaluationCount++;
            return evaluator.evaluate(player, local);
            }
        }

        int highestRating = Integer.MIN_VALUE;
        while (generator.hasNext()) {
            if (super.isTimeUp()) {
                return highestRating;
            }
            Move move = generator.next();
            local = move.getMapAfterMove();

            int nextPlayer = getNextPly(player);
            highestRating = Math.max(highestRating,
                    this.getMinValue(depth - 1, local, nextPlayer, 0));

            local = move.getMapAfterUndoMove();//important to undo the Move
        }
        return highestRating;
    }

    private int getNextPly(int player) {
        return ((player) % map.getPlayerCount()) + 1;
    }
    @Override
    public String getName() {
        return "Minimax";
    }

    public static void main(String[] args) {
        Map map = MapFactory.createFromFile("./maps/phase1/map8_midgame.map");//MapFactory.createFromFile("./maps/phase1/2TurnInARow.map");
        int player = 1;
        System.out.println(map);
        SearchStrategy search = new MiniMax(map, player, new SimpleEvaluation());
        int repeatCount = 1;
        int depth = 2;
        Move move = null;
        long time = 0;
        for (int i = 0; i < repeatCount; i++) {
            long start = System.currentTimeMillis();
            move = search.getMove(depth, Long.MAX_VALUE);
            time += System.currentTimeMillis() - start;
        }
        
        System.out.println(move + " " + search.getNodeCount() + " Avg time ms" + time / repeatCount);
    }

}
