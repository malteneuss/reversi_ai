package ai.search.aspirationWindows;

import ai.evaluation.Evaluation;
import ai.evaluation.SimpleEvaluation;
import ai.search.AlphaBeta;
import ai.search.CountingSearch;
import ai.search.IterativeDeepening;
import ai.search.MiniMax;
import ai.search.SearchStrategy;
import factories.EvaluationFactory;
import factories.MapFactory;
import factories.MoveFactory;
import factories.SearchStrategyFactory;
import map.Map;
import move.Move;

public class ItDeepeningAspWindows extends CountingSearch {
	public static int alpha=Integer.MIN_VALUE;
	public static int beta=Integer.MAX_VALUE;
	public static int bestVal=0;
    private AlphaBeta search;
    private int startedDepth;
    private int factor=1;
    private int restartCount=0;
    public ItDeepeningAspWindows(Map m, int player, Evaluation ev, int factor) {
       search=new AlphaBeta(m,player,ev);
       this.factor=factor;
    }

    public ItDeepeningAspWindows(Map m, int player, Evaluation ev) {
        this(m, player, ev, 1);//TODO CHECK is this 1 correct
    }

    @Override
    public Move getMove(int depth, long timeLeftInMs) {
        boolean failed=false;
    	long startTime = System.currentTimeMillis();

        super.resetEvalAndNodeCounters();
        super.restartClock(timeLeftInMs);

        super.bestMove = MoveFactory.getInvalidMove(Integer.MIN_VALUE);//???
       
        if (depth == 0) {
            return search.getMove(depth, timeLeftInMs);
        }

        Move tempMove;
        for (startedDepth = 1; startedDepth <= depth; startedDepth++) {
            if (super.isTimeUp()) {
                return super.bestMove;
            }
            //TODO decide if time is enough to try next depth
            tempMove = search.getMove(startedDepth, timeLeftInMs - (System.currentTimeMillis() - startTime),alpha,beta);
            alpha=search.getAlpha();
            beta=search.getBeta();
            bestVal=super.bestMove.getRating();
            
            if(bestVal<=alpha&&!failed) {//fail low
            	failed=true;
            	//System.out.println("fail-low occured. reseting alpha");
            	alpha=-10000;
            	beta=bestVal-200;
            	restartCount++;
            	continue;//start new
            }
            if(bestVal>=beta&&!failed) {//fail high
            	failed=true;
            	//System.out.println("fail-low occured. reseting beta");
            	alpha=bestVal-200;
            	beta=10000;
            	restartCount++;
            	continue;//start new
            }
            if(failed&&(bestVal>=beta&&bestVal<=alpha)) {//double fail
            	//System.out.println("double fail. reseting alpha/beta");
            	alpha=-10000;
            	beta=10000;
            	startedDepth--;
            	restartCount++;
            	continue;//start new
            }
            
            if(bestVal==-10000||bestVal==10000) {//lost/won
            	//System.out.println("won/lose. adjusting alpha/beta");
            	alpha=bestVal-100;
            	beta=bestVal+100;
            }
            if(bestVal>=0&&bestVal<=50) {//prob gonna lose
            //	System.out.println("prob going to lose. adjusting alpha/beta");
            	alpha=bestVal-200;
            	beta=bestVal;
            }
            if(bestVal>100&&bestVal<=800) {//prob gonna win
            	//System.out.println("prob going to win. adjusting alpha/beta");
            	alpha=bestVal;
            	beta=bestVal+200;
            }
            
            if (tempMove.getRating() >= bestMove.getRating()) {
                super.bestMove = tempMove;
            }
            super.nodeCount += search.getNodeCount();
            super.evaluationCount += search.getEvalCount();
        }
        return bestMove;
    }

    public int getRestartCount() {
    	System.out.println(this.restartCount);
    	return this.restartCount;
    }
    @Override
    public String getName() {
        return "Aspiration Windows with " + search.getName();
    }

    public int getFinishedDepth() {
        return this.startedDepth - 1;
    } 
}
