package ai.search;

import ai.evaluation.Evaluation;
import factories.MoveGeneratorFactory;
import java.util.ArrayList;

import java.util.List;
import map.Map;
import move.Move;
import movegenerator.MoveGenerator;

public class RandomSearch implements SearchStrategy {

    private Map map;
    private int playerNumber;

    
    public RandomSearch(Map map, int ownPlayerNumber, Evaluation e) {
        this.map = map;
        this.playerNumber = ownPlayerNumber;
    }

    @Override
    public Move getMove(int depth, long startDuration) {

        
        List<Move> moves = new ArrayList<>();
        MoveGenerator generator = MoveGeneratorFactory.getFrom(map, playerNumber);
        while (generator.hasNext()) {
            moves.add(generator.next());
        }
        int moveNumber = (int) (Math.random() * moves.size());
        return moves.get(moveNumber);
    }

    @Override
    public long getNodeCount() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getEvalCount() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String getName() {
        return "RandomSearch";
    }
}
