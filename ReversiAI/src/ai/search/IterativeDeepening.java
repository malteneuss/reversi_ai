package ai.search;

import factories.MoveFactory;
import move.Move;

public class IterativeDeepening extends CountingSearch {

    private SearchStrategy search;
    private int startedDepth;

    public IterativeDeepening(SearchStrategy moveFinder) {
        this.search = moveFinder;
    }

    @Override
    public Move getMove(int depth, long timeForAllIterationsInMs) {
        long startTime = System.currentTimeMillis();
        super.resetEvalAndNodeCounters();
        super.restartClock(timeForAllIterationsInMs);
        //
        super.bestMove = MoveFactory.getInvalidMove(Integer.MIN_VALUE);

        if (depth == 0) {
            return search.getMove(depth, timeForAllIterationsInMs);
        }

        Move tempMove;
        for (startedDepth = 1; startedDepth <= depth; startedDepth++) {
            long timeUsed = (System.currentTimeMillis() - startTime);
            long timeLeft = timeForAllIterationsInMs - timeUsed;
            if ((3 * timeUsed) > timeLeft) { //3 because average branching factor should be >=4
                return super.bestMove;
            }
       
            tempMove = search.getMove(startedDepth, timeLeft);
            if (tempMove.getRating() >= bestMove.getRating()) {
                super.bestMove = tempMove;
            }
            super.nodeCount += search.getNodeCount();
            super.evaluationCount += search.getEvalCount();
        }
        return bestMove;
    }

    @Override
    public String getName() {
        return "Iterative Deepening with " + search.getName();
    }

    public int getFinishedDepth() {
        return this.startedDepth - 1;
    }
}
