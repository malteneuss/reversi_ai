package move.helper;

import ai.evaluation.experiment.ComplexEvaluation;
import factories.MapFactory;
import map.Map;
import java.util.HashSet;
import java.util.Set;

public class Bomber {

    public static Map detonate(Map map, int x, int y, int radius) {
        Set<TilePosition> toDestroy = new HashSet<>();
        toDestroy = collectTilesToBeDestroyedInAllDirections(toDestroy, map, x, y, radius);
        for (TilePosition t : toDestroy) {
            map.setChar(Map.HOLE, t.getX(), t.getY());
        }
        return map;
//        bomb(map, x, y, radius);
//        for (int yn = 0; yn < map.getHeight(); yn++) {
//            for (int xn = 0; xn < map.getWidth(); xn++) {
//                if (map.getChar(xn, yn) == 't') {
//                    map.setChar(Map.HOLE, xn, yn);
//                }
//            }
//        }
//        return map;
    }

    private static void bomb(Map map, int x, int y, int steps) {
        char con = map.getChar(x, y);
        if (con != '-') {
            map.setChar('t', x, y);



            for (int d = 0; d < 8; d++) {

                int[] pd = ComplexEvaluation.goIntoDirection(x, y, d);//map.goOneStepInLineAlong(new PositionDirection(x, y, Direction.intIntoDirection(d)));

                if (map.isOnMap(pd[0], pd[1]) && map.getChar(pd[0], pd[1]) != Map.HOLE) {
                    if (steps > 0) {
                        bomb(map, pd[0], pd[1], steps - 1 );
                    }
                } else {
                    pd = map.getTransitionEndpoint(x, y, d);
                    if (pd != null && steps > 0) {
                        bomb(map, pd[0], pd[1], steps - 1);
                    }
                }
            }
        }
    }


    public static Set<TilePosition> collectDetonatedPosition(Map map, int x, int y, int radius) {
        Set<TilePosition> toDestroy = new HashSet<>();
        toDestroy = collectTilesToBeDestroyedInAllDirections(toDestroy, map, x, y, radius);
        for (TilePosition t : toDestroy) {
            map.setChar(t.getChar(), t.getX(), t.getY());
        }
        return toDestroy;
    }

    /**
     * Collects tiles within the bombs blast radius. Will also check around
     * corners
     */
    private static Set<TilePosition> collectTilesToBeDestroyedInAllDirections(Set<TilePosition> destroy, Map map, int x, int y, int radius) {
        if (map.isBombableAt(x, y) && map.getChar(x, y) < 'z' + radius) {
            TilePosition current = new TilePosition(map.getChar(x, y), x, y);
            destroy.add(current);
            map.setChar((char) ('z' + radius), x, y);//mark with radius
            if (radius > 0) {
                for (int d = 0; d < 8; d++) {
                    int[] pd = map.goOneStepInLineAlong(
                            RecolorCounter.createPositionDirection(x, y, d));
                    if (pd != null) {
                        destroy = collectTilesToBeDestroyedInAllDirections(
                                destroy, map, pd[0], pd[1], radius - 1);
                    }
                }
            }
            //map.setChar(current.getChar(), x, y);//restore
        }
        return destroy;
    }

    public static void main(String[] args) {
        String mapAsString = ""//
                + "8\n" //8 player
                + "1\n"//1 Override
                + "1 5\n"//1 Bomb
                + "4 6\n"// Map
                //
                + "0 0 0 0 - -\n"//
                + "0 - - x - 0\n"//
                + "0 - - - - -\n"//
                + "0 0 0 0 0 -\n"//
                //Transition
                + "4 3 7 <-> 5 1 0\n"//4 3 UP_Left 5 1 UP
                + "0 1 3 <-> 4 3 4\n"//0 1 DOWN_Right 4 3 DOWN
                ;

        //Map map = MapFactory.createFromString(mapAsString);
        Map map = MapFactory.createFromFile("./maps/phase2/RoundedMap.map");
        int player = 1;
        // System.out.println(map);
//        Set<TilePosition> set = Bomber.collectDetonatedPosition(map, 3, 3, 3);
//        for (TilePosition t : set) {
//            System.out.println(t);
//        }
//
//        System.out.println(Bomber.detonate(map, 3, 3, 3).toString());
        long start = System.currentTimeMillis();
        Set<TilePosition> tiles = collectDetonatedPosition(map, 15, 15, 8);
        //Map map2 = detonate(map, 3, 1, 5);//
        Map map2 = detonate(map, 15, 15, 8);
        System.out.println(System.currentTimeMillis() - start);
       
        System.out.println(map2.toString() + tiles.size());


    }
}
