package move.helper;

import map.Map;

public class Recolorer {

    public static Map recolorAllDirectionsIgnoreInitial(int player, Map map, int x, int y, int[] toRecolorCounts) {
        for (int direction = 0; direction < 8; direction++) {//All Directions 0-7
            int[] pd = RecolorCounter.createPositionDirection(x, y, direction);
     
            if (toRecolorCounts[direction] > 0) {
                map = recolorInLineAlong(player, map, pd, toRecolorCounts[direction]);
            }
        }
        return map;
    }

    /*
     * There is something to recolor if in at least one
     * direction/position in the array there is a step to
     */
    public static boolean hasStonesToRecolor(int[] toRecolorSteps) {
        for (int i = 0; i < toRecolorSteps.length; i++) {
            if (toRecolorSteps[i] > 0) {
                return true;
            }
        }
        return false;
    }

    /*
     * Recolors a set of stones defined by recolorCount in a single line and direction.
     * It ignores the starting point (x,y)
     * Beeing player 1 and recoloring in 2 2 2 1 starting from the left '2' a recolor count of
     * 1 will ignore the first/left '2' and result
     * in 2 1 2 1. In order to color all the '2's one need to pass 2 as a recolor count.
     * The correct recolor count of enclosing stones can be computed by getRecolorCountInLineAlong
     * @param toRecolorCount the precise number of stones to be recolored has to be valid and
     * not lead out of map
     */
    private static Map recolorInLineAlong(int player, Map map, int[] pd, int toRecolorCount) {
        pd = map.goOneStepInLineAlong(pd);//ignore initial
        while (toRecolorCount > 0) {
            map.setPlayerAt(player, pd[0], pd[1]);
            pd = map.goOneStepInLineAlong(pd);
            toRecolorCount--;
        }
        return map;
    }
}
