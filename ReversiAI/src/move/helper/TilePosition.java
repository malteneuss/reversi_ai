/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package move.helper;

public class TilePosition {

    private char c;
    private int x;
    private int y;

    public TilePosition(char c, int x, int y) {
        this.c = c;
        this.x = x;
        this.y = y;
    }

    public char getChar() {
        return c;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.c;
        hash = 79 * hash + this.x;
        hash = 79 * hash + this.y;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TilePosition other = (TilePosition) obj;
        if (this.c != other.c) {
            return false;
        }
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TilePosition{" + "c=" + c + ", x=" + x + ", y=" + y + '}';
    }
}
