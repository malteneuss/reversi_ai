package move.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import map.Map;

public class RecolorCounter {

    /**
     * Returns a list of all positions to recolor if a stone of player is placed
     * at (x,y). The list is empty if nothing to recolor. It does not include
     * the initial position (x,y) , so that a invalid move can be checked by
     * list.isEmpty(). It searches each direction 0-7 until a stone of 'player'
     * is found and collects all positions to recolor.
     */
    public static List<TilePosition> collectAllRecolorPositions(int player, Map map, int x, int y) {
        List<TilePosition> recolorPositions = new ArrayList<>();
        char restore = map.getChar(x, y);//important to prevent loops, is very fast
        //recolorPositions.add(new TilePosition(restore, x, y));
        map.setChar(Map.MARKED, x, y);
        int[] pd;
        for (int direction = 0; direction < 8; direction++) {
            pd = createPositionDirection(x, y, direction);
            recolorPositions.addAll(collectAllRecolorPositionsInLineAlong(player, map, pd));
        }
        map.setChar(restore, x, y);
        return recolorPositions;
    }

    /**
     * Adds all positions to recolor along a line defined by PositionDirection
     * pd to the recolorPositions list.
     *
     * @param pd starting position will not be checked and direction
     */
    private static List<TilePosition> collectAllRecolorPositionsInLineAlong(
            int player, Map map, int[] pd) {
        List<TilePosition> recolorPositions = new ArrayList<>();
        int recolorCountInLine = 0;
        pd = map.goOneStepInLineAlong(pd);//ignore initial (x,y)
        while (pd != null && (map.isOverrideableAt(pd[0], pd[1]))) {
            if (map.isPlayerAt(player, pd[0], pd[1])) {
                if (recolorCountInLine > 0) {//at least one enclosing enemy stone
                    return recolorPositions;
                } else {
                    return Collections.EMPTY_LIST;
                }
            }
            recolorPositions.add(new TilePosition(map.getChar(pd[0], pd[1]), pd[0], pd[1]));
            recolorCountInLine++;
            pd = map.goOneStepInLineAlong(pd);
        }
        //at this position pd is either out of map or hole/empty
        //then there is nothing to enclose
        return Collections.EMPTY_LIST;
    }


    /**
     * It ignores the initial position (x,y) and searches each direction 0-7
     * until a stone of 'player' is found. The result is then the number of
     * enemy or expansion stones along the way from (x,y) to 'player'. In case
     * it encounters a non-overrideable tile or gets out of map along a
     * direction, then the color count in this direction is 0. Counting is
     * surely more efficient than collecting the precise positions of the stones
     * to recolor.
     *
     * @param player
     * @param x
     * @param y
     * @return
     */
    public static int[] getForAllDirections(int player, Map map, int x, int y) {
        int[] recolorCounts = new int[8];//default 0 for all positions
        char restore = map.getChar(x, y);//important to prevent loops, is very fast
        map.setChar(Map.MARKED, x, y);
        int[] pd;
        for (int direction = 0; direction < 8; direction++) {
            pd = createPositionDirection(x, y, direction);
            recolorCounts[direction] = getRecolorCountInLineAlong(player, map, pd);
        }
        map.setChar(restore, x, y);
        return recolorCounts;
    }

    /**
     *
     * @param pd starting position will not be checked and direction
     * @return 0 means no stones to recolor in this direction, 1 means there is
     * one enemy stone enclosed like in 1 2 1 if you start at the left '1'
     */
    private static int getRecolorCountInLineAlong(int player, Map map, int[] pd) {
        assert (pd != null);
        assert (map != null);
        int recolorCountInLine = 0;
        pd = map.goOneStepInLineAlong(pd);//ignore initial (x,y)
        while (pd != null && (map.isOverrideableAt(pd[0], pd[1]))) {
            if (map.isPlayerAt(player, pd[0], pd[1])) {
                if (recolorCountInLine > 0) {//at least one enclosing enemy stone
                    return recolorCountInLine;
                } else {
                    return 0;
                }
            }
            recolorCountInLine++;
            pd = map.goOneStepInLineAlong(pd);
        }
        //at this position pd is either out of map or hole/empty
        //then there is nothing to enclose
        return 0;
    }

    public static int[] createPositionDirection(int x, int y, int direction) {
        int[] pd = new int[3];
        pd[0] = x;
        pd[1] = y;
        pd[2] = direction;
        return pd;
    }
}
