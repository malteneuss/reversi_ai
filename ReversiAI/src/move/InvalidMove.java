package move;

import map.Map;

public class InvalidMove extends Move {

    public InvalidMove(int rating) {
        super.player = -1;
        super.x = -1;
        super.y = -1;
        super.special = - 1;
        super.rating = rating;
    }

    @Override
    public Map getMapAfterMove() {
        throw new UnsupportedOperationException("InvalidMove is not allowed to be executed.");
    }

    @Override
    public Map getMapAfterUndoMove() {
        throw new UnsupportedOperationException("InvalidMove is not allowed to be executed.");
    }
}
