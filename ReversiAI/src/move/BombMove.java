package move;

import map.Map;
import factories.MapFactory;
import move.helper.Bomber;

/**
 * This move implementation is specified for the bomb phase
 */
public class BombMove extends Move {

    private Map oldMap;

    public BombMove(Map map, int player, int x, int y) {
        this.oldMap = map;
        super.map = map.copy();
        super.player = player;
        super.x = x;
        super.y = y;
        super.special = 0;
    }

    @Override
    public Map getMapAfterMove() {
        return Bomber.detonate(super.map, x, y, map.getBombStrength());
    }

    @Override
    public Map getMapAfterUndoMove() {
        return oldMap;
    }

    public static void main(String[] args) {
        Map map = MapFactory.createFromFile("./maps/minimalTest.map");
        int player = 1;
        System.out.println("Startmap:\n" + map);
        Move move = new BombMove(map, player, 2, 2);
        System.out.println(move);
        System.out.println("After Move ");
        System.out.println(move.getMapAfterMove());
    }
}
