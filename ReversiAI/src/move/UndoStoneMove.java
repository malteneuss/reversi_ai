package move;

import factories.MapFactory;
import java.util.List;
import map.Map;
import move.helper.TilePosition;
import movegenerator.AllAtOnceMoveGenerator;
import movegenerator.MoveGenerator;

public class UndoStoneMove extends Move {

    private List<TilePosition> recolorPositions;
    private char oldCharAtXY;


    public UndoStoneMove(Map map, int player, int x, int y, int special, List<TilePosition> recolorPositions) {
        super.map = map;
        super.player = player;
        super.x = x;
        super.y = y;
        super.special = special;
        this.recolorPositions = recolorPositions;
    }

    @Override
    public Map getMapAfterMove() {
        oldCharAtXY = map.getChar(x, y);

        if (map.isAnyPlayerAt(x, y)) {
            map.setOverrideCountOf(getPlayer(), map.getOverrideCountOf(getPlayer()) - 1);
        }
        for (TilePosition t : recolorPositions) {
            map.setPlayerAt(player, t.getX(), t.getY());
        }
        map.setPlayerAt(player, x, y);
      

        applySpecialRules(oldCharAtXY, getPlayer(), special);

        return map;
    }

    @Override
    public Map getMapAfterUndoMove() {
        revertSpecialRules(oldCharAtXY, player, special);
        //restore field
        map.setChar(oldCharAtXY, x, y);
        for (TilePosition t : recolorPositions) {
            map.setChar(t.getChar(), t.getX(), t.getY());
        }

        //if override stone was use, restore overridecounts
        if (map.isAnyPlayerAt(x, y)) {
            map.setOverrideCountOf(getPlayer(), map.getOverrideCountOf(getPlayer()) + 1);
        }
        return map;
    }

    private void applySpecialRules(char tileAtXY, int player, int special) {
        //now apply special rules
        if (tileAtXY == 'b') {//bonus rule
            if (special == Map.BONUS_BOMB) {
                map.setBombCountOf(player, map.getBombCountOf(player) + 1);
            } else {
                map.setOverrideCountOf(player, map.getOverrideCountOf(player) + 1);
            }
        } else if (tileAtXY == 'x') {//Expansion rule
            map.setOverrideCountOf(player, map.getOverrideCountOf(player) - 1);
        } else if (tileAtXY == 'c') {//swap rule
            //special == swapChoice
            map.swap(player, special);
        } else if (tileAtXY == 'i') {//inversion
            map.inverse();
        }
    }

    private void revertSpecialRules(char tileAtXY, int player, int special) {
        //now revert special rules
        if (tileAtXY == 'b') {//bonus rule
            if (special == Map.BONUS_BOMB) {
                map.setBombCountOf(player, map.getBombCountOf(player) - 1);
            } else {
                map.setOverrideCountOf(player, map.getOverrideCountOf(player) - 1);
            }
        } else if (tileAtXY == 'x') {//Expansion rule
            map.setOverrideCountOf(player, map.getOverrideCountOf(player) + 1);
        } else if (tileAtXY == 'c') {//swap rule
            //special == swapChoice
            map.swap(player, special);
        } else if (tileAtXY == 'i') {//inversion
            map.reverse();
        }
    }

    public static void main(String[] args) {
        Map map = MapFactory.createFromFile("./maps/phase1/minimalTest.map");
        int player = 1;
        System.out.println("Startmap:\n" + map);
        MoveGenerator gen = new AllAtOnceMoveGenerator(map, player);
        while (gen.hasNext()) {
            Move move = gen.next();
            System.out.println(move);
            System.out.println("After Move ");
            //move.getMapAfterMove();
            System.out.println(move.getMapAfterMove());
            //System.out.println(move.getMapAfterUndoMove());
        }
    }
}
