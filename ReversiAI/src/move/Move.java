package move;

import map.Map;

/**
 * 
 */
public abstract class Move implements Comparable {

    
    protected int player;
    protected int x;
    protected int y;
    protected int special;
    protected int rating;
    //
    protected Map map;

    public Move(){}
    /**
     * 
     * Precondition: this method was not called before
     * @return
     */
    public abstract Map getMapAfterMove();

    public abstract Map getMapAfterUndoMove();

    public int getPlayer() {
        return player;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getSpecial() {
        return special;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getRating() {
        return rating;
    }

    @Override
    public String toString() {
        return "NewMove{" + "player=" + player + ", x=" + x + ", y=" + y + ", special=" + special + ", rating=" + rating + '}';
    }

    @Override
    public int compareTo(Object other) {
        if (other instanceof Move) {
            return this.rating - ((Move) other).rating;
        }
        return -1;
    }
}
