package move;

import move.helper.Recolorer;
import movegenerator.MoveGenerator;
import factories.MapFactory;
import movegenerator.AllAtOnceMoveGenerator;
import map.Map;

/**
 * This move implementation is specified for phase one. Undo is implemented with
 * copies of maps
 */
public class CopyingStoneMove extends Move {

    private Map oldMap;
    private int[] recolorCounts;

    public CopyingStoneMove(Map map, int player, int x, int y, int special, int[] recolorCounts) {
        this.oldMap = map;
        super.map = map.copy();
        super.player = player;
        super.x = x;
        super.y = y;
        super.special = special;
        this.recolorCounts = recolorCounts;
    }

    /**
     * sets a players stone on a tile, computes the recoloring. It uses the
     * expansion rule, awards bonuses, does swapping and so forth, but does no
     * checks at all to increase performance OR Sets all tiles to Hole which are
     * reachable within the radius of the blast through non-Hole tiles and
     * decrements the bomb count of playerNumber No checks for performance it
     * MUST BE A VALID MOVE!!
     *
     * @param playerNumber must be 1-8
     */
    @Override
    public Map getMapAfterMove() {
        char tileAtXY = super.map.getChar(x, y);

        //recolor all but (x,y)
        if (Recolorer.hasStonesToRecolor(recolorCounts)) {
            super.map = Recolorer.recolorAllDirectionsIgnoreInitial(getPlayer(), super.map, x, y, recolorCounts);
            if (map.isAnyPlayerAt(x, y)) {
                map.setOverrideCountOf(getPlayer(), map.getOverrideCountOf(getPlayer()) - 1);
            }
        }

        //now recolor (x,y)
        map.setPlayerAt(getPlayer(), x, y);
        applySpecialRules(tileAtXY, getPlayer(), special);
        return super.map;
    }

    @Override
    public Map getMapAfterUndoMove() {
        return oldMap;
    }

    private void applySpecialRules(char tileAtXY, int player, int special) {
        //now apply special rules
        if (tileAtXY == 'b') {//bonus rule
            if (special == Map.BONUS_BOMB) {
                map.setBombCountOf(player, map.getBombCountOf(player) + 1);
            } else {
                map.setOverrideCountOf(player, map.getOverrideCountOf(player) + 1);
            }
        } else if (tileAtXY == 'x') {//Expansion rule
            map.setOverrideCountOf(player, map.getOverrideCountOf(player) - 1);
        } else if (tileAtXY == 'c') {//swap rule
            //special == swapChoice
            map.swap(player, special);
        } else if (tileAtXY == 'i') {//inversion
            map.inverse();
        }
    }

    public static void main(String[] args) {
        Map map = MapFactory.createFromFile("./maps/minimalTest.map");
        int player = 1;
        System.out.println("Startmap:\n" + map);
        MoveGenerator gen = new AllAtOnceMoveGenerator(map, player);
        while (gen.hasNext()) {
            Move move = gen.next();
            System.out.println(move);
            System.out.println("After Move ");
            System.out.println(move.getMapAfterMove());

        }
    }
}
