package movegenerator;

import move.Move;

/**
 * Generates only valid moves for a map
 * Stores information how to compute the next move
 */
public interface MoveGenerator {

    /**
     * True if underlying player has a valid move left on underlying map
     */
    public abstract boolean hasNext();
  
    public abstract Move next();

    public abstract int getLength();
  
}
