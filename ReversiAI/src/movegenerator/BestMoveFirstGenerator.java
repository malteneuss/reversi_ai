package movegenerator;

import factories.MapFactory;
import move.Move;
import move.helper.RecolorCounter;
import move.helper.Recolorer;
import factories.MoveFactory;
import map.Map;
import java.util.LinkedList;
import java.util.Queue;
import core.StaticInfo;

public class BestMoveFirstGenerator implements MoveGenerator {

    
    Queue<Move> betterMoves;
    Queue<Move> moves;
    private Map map;
    private int player;

    public BestMoveFirstGenerator(Map map, int player) {
        this.map = map;
        this.player = player;
        betterMoves = new LinkedList<>();
        moves = new LinkedList<>();
        collectAllMoves();
    }

    @Override
    public boolean hasNext() {
        return !moves.isEmpty();
    }

    @Override
    public Move next() {
        return moves.remove();
    }

    public int getLength() {
    	return moves.size();
    }
    private final void collectAllMoves() {
        for (int y = 0; y < map.getHeight(); y++) {
            for (int x = 0; x < map.getWidth(); x++) {

                if (canPlaceOn(x, y)) {

                    int[] recolorCounts =
                            RecolorCounter.getForAllDirections(player, map, x, y);
                    if ((map.isFree(x, y) || map.isAnyPlayerAt(x, y)) && !Recolorer.hasStonesToRecolor(recolorCounts)) {
                        //if not expansion rule it has to have something to recolor! 
                        continue;
                    }
                    int index = 0;//default for no special Tile
                    if (map.isBonusAt(x, y)) {
                        index = StaticInfo.bonusIndex;
                    } else if (map.isChoiceAt(x, y)) {
                        index = StaticInfo.playerSwapIndex;
                    }

                    for (int specialIndex = 0; specialIndex < StaticInfo.validSpecials[index].length; specialIndex++) {
                        moves.add(MoveFactory.getRegularMoveFrom(
                                map, player, x, y, StaticInfo.validSpecials[index][specialIndex], recolorCounts));
                    }
                }
            }
        }
    }



    /**
     * A player can place a stone on a tile if it is Free or used but player has
     * an override stone Caution: doesn't check wether any other tiles would be
     * recolored which is neccessary for placing a stone on a free tile or on
     * another player
     */
    private boolean canPlaceOn(int x, int y) {
        //check for expansion rule aswell
        return map.isFree(x, y)
                || ((map.isAnyPlayerAt(x, y) || map.isExpansionAt(x, y))
                && map.getOverrideCountOf(player) > 0);
    }

    public static void main(String[] args) {
        Map map = MapFactory.createFromFile("./maps/minimalTest.map");
        int player = 1;
        System.out.println(map);
        MoveGenerator gen = new BestMoveFirstGenerator(map, player);
        while (gen.hasNext()) {
            System.out.println(gen.next());
        }
    }
}
