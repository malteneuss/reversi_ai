package movegenerator;

import factories.MapFactory;
import factories.MoveFactory;
import move.Move;
import move.helper.RecolorCounter;
import map.Map;

public class IterativeMoveGenerator implements MoveGenerator {

    private Map map;
    private int player;
    private int lastX;
    private int lastY;
    private int lastSpecialIndex;
    private int nextX;
    private int nextY;
    private int nextSpecialIndex;
    private int[] recolorCounts;
    private boolean moveFound;
 
    
    public IterativeMoveGenerator(Map map, int player){
        this.map = map;
        this.player = player;
        lastX = 0;
        lastY = 0;
        lastSpecialIndex = 0;
        nextX = 0;
        nextY = 0;
        nextSpecialIndex = 0;
        
        
    }

   
    @Override
    public boolean hasNext() {
        
        int x = lastX;
        int y = lastY;
        
        
        // no other move allready found
        if(!moveFound){
                       
            while(y < map.getHeight() && !moveFound){

                if(y > lastY){
                    x = 0;
                }
                
                
                while(x < map.getWidth() && !moveFound){
                    
                    if(canPlaceOn(x, y)){
                       

                        
                        recolorCounts = RecolorCounter.getForAllDirections(player, map, x, y);
                        
                        boolean possibleCaputreMove = false;
                        for(int i = 0; i < recolorCounts.length; i++){
                            if(recolorCounts[i]!=0)
                                possibleCaputreMove = true;
                        }
                        
                        if(possibleCaputreMove){
                                                        
                            // normal move without any specials
                            if(map.isEmptyAt(x, y) && (lastX != x || lastY != y)){
                                nextX = x;
                                nextY = y;
                                nextSpecialIndex = 0;
                                moveFound = true;
                            }
                            
                            else if(map.isBonusAt(x, y) && lastSpecialIndex < 21 ){
                                nextX = x;
                                nextY = y;
                                if(lastSpecialIndex < 20)
                                    nextSpecialIndex = 20;
                                else
                                    nextSpecialIndex = 21;
                                
                                moveFound = true;                               
                            }
                            // there is a choice field on x,y
                            else if(map.isChoiceAt(x, y) && (lastSpecialIndex != 8) ){
                                
                                
                                if(lastSpecialIndex < 1 || lastSpecialIndex > 8){
                                    if(player != 1){
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 1;
                                        moveFound = true;
                                    }
                                    else{
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 2;
                                        moveFound = true;                                        
                                    }
                                    
                                }
                                else if(lastSpecialIndex == 1){
                                    if(player != 2){
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 2;
                                        moveFound = true;
                                    }
                                    else if(map.getPlayerCount() > 2){
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 3;
                                        moveFound = true;
                                    }
                                        
                                }
                                else if(lastSpecialIndex == 2){
                                    if(player != 3 && map.getPlayerCount() > 2){
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 3;
                                        moveFound = true;
                                    }
                                    else if(map.getPlayerCount() > 3){
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 4;
                                        moveFound = true;
                                    }
                                        
                                }
                                else if(lastSpecialIndex == 3){
                                    if(player != 4 && map.getPlayerCount() > 3){
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 4;
                                        moveFound = true;
                                    }
                                    else if(map.getPlayerCount() > 4){
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 5;
                                        moveFound = true;
                                    }
                                }
                                else if(lastSpecialIndex == 4){
                                    if(player != 5 && map.getPlayerCount() > 4){
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 5;
                                        moveFound = true;
                                    }
                                    else if(map.getPlayerCount() > 5){
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 6;
                                        moveFound = true;                                        
                                    }
                                }
                                else if(lastSpecialIndex == 5){
                                    if(player != 6 && map.getPlayerCount() > 5){
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 6;
                                        moveFound = true;
                                    }
                                    else if(map.getPlayerCount() > 6){
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 7;
                                        moveFound = true;
                                    }
                                }
                                else if(lastSpecialIndex == 6){
                                    if(player != 7 && map.getPlayerCount() > 6){
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 7;
                                        moveFound = true;
                                    }
                                    else if(map.getPlayerCount() > 7){
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 8;
                                        moveFound = true;
                                    }
                                }
                                else if(lastSpecialIndex == 7){
                                    if(player != 8) {
                                        nextX = x;
                                        nextY = y;
                                        nextSpecialIndex = 8;
                                        moveFound = true;
                                    }
                                }
                            }
                            
                            
                            // there is a player
                            else if(!map.isPlayerAt(player, x, y) && (lastX != x || lastY != y) && map.getBombCountOf(player) > 0){
                                nextX = x;
                                nextY = y;
                                nextSpecialIndex = 0;
                                moveFound = true;                         
                            }
                        }
                        
                        // there is an expansionsfield
                        if(map.isExpansionAt(x, y) && map.getOverrideCountOf(player) > 0 
                                && (lastX != x || lastY != y)){
                            nextX = x;
                            nextY = y;
                            nextSpecialIndex = 0;
                            moveFound = true;
                        }
                        
                    }
                    
                    
                    x++;
                }
                
                y++;
            }
        }
     // there was another move allready found
        else
            moveFound = true;
        
        return moveFound;
    }
    
    
    /**
     * A player can place a stone on a tile if it is Free or used but player has
     * an override stone Caution: doesn't check wether any other tiles would be
     * recolored which is neccessary for placing a stone on a free tile or on
     * another player
     */
    private boolean canPlaceOn(int x, int y) {
        //check for expansion rule aswell
        return map.isFree(x, y)
                || (!map.isPlayerAt(player, x, y) && ((map.isAnyPlayerAt(x, y) || map.isExpansionAt(x, y)))
                && map.getOverrideCountOf(player) > 0);
    }

    @Override
    public Move next() {
        lastX = nextX;
        lastY = nextY;
        lastSpecialIndex = nextSpecialIndex;
        moveFound = false;
        return MoveFactory.getMoveForMapUpdate(map, player, lastX, lastY, lastSpecialIndex);
    }
    
    public static void main(String[] args) {
        String mapAsString = ""//
                + "8\n" //8 player
                + "1\n"//1 Override
                + "1 3\n"//1 Bomb
                + "5 5\n"//5 x 5 Map
                //
                + "1 1 0 1 6\n"//
                + "1 2 3 7 0\n"//
                + "x 4 5 - 8\n"//
                + "- x i b -\n"//
                + "0 c - x 0\n"//
                //Transition
                + "4 0 2 <-> 4 4 1\n"; //4 0 Right - 4 4 UP_Right
        Map map = MapFactory.createFromString(mapAsString);

        int player = 1;
        System.out.println(map);
        MoveGenerator gen = new IterativeMoveGenerator(map, player);
        while (gen.hasNext()) {
            System.out.println(gen.next());
        }
    }
    


	@Override
	public int getLength() {
		return -1;
	}

}
