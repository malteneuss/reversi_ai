package movegenerator;

import factories.MapFactory;
import move.Move;
import move.helper.RecolorCounter;
import move.helper.Recolorer;
import factories.MoveFactory;
import map.Map;
import core.StaticInfo;
import java.util.PriorityQueue;
import map.StaticInfos;

public class IterativeOverrideJustWhenNecessaryGenerator implements MoveGenerator {

    PriorityQueue<Move> moves;
    PriorityQueue<Move> overrideMoves;
    private Map map;
    private int player;
    private boolean hadNormalMoves = false;
    private int startX = 0;
    private int startY = 0;

    public IterativeOverrideJustWhenNecessaryGenerator(Map map, int player) {
        this.map = map;
        this.player = player;
        moves = new PriorityQueue<>();
        overrideMoves = new PriorityQueue<>();
    }

    @Override
    public boolean hasNext() {
        //if there are moves in the queue, 
     
        //if there was a normalmove, overridemoves should be ignored
        if (hadNormalMoves) {
            if (moves.isEmpty()) {
                findNextNormalMoveButCollectOverridesAlso();
            }
            return !moves.isEmpty();
        } else {//if no normalmove found yet
            findNextNormalMoveButCollectOverridesAlso();
            if (moves.isEmpty()) {//if there still no normal moves then are none alltogether
                return !overrideMoves.isEmpty();
            } else {
                return true;
            }
        }
    }

    @Override
    public Move next() {
        Move move;
        if (hadNormalMoves) {
            move = moves.remove();
        } else {
            move = overrideMoves.remove();
        }
        move.setRating(0);//remove prerating
        return move;
    }

    public int getLength() {
        return moves.size();
    }

    /**
     * A player can place a stone on a tile if it is Free or used but player has
     * an override stone Caution: doesn't check wether any other tiles would be
     * recolored which is neccessary for placing a stone on a free tile or on
     * another player
     */
    private boolean canPlaceOn(int x, int y) {
        //check for expansion rule aswell
        return map.isFree(x, y)
                || ((map.isAnyPlayerAt(x, y) || map.isExpansionAt(x, y))
                && map.getOverrideCountOf(player) > 0);
    }

    public static void main(String[] args) {
        Map map = MapFactory.createFromFile("./maps/phase1/minimalTest.map");
        int player = 1;
        System.out.println(map);
        MoveGenerator gen = new IterativeOverrideJustWhenNecessaryGenerator(map, player);
        while (gen.hasNext()) {
            System.out.println(gen.next());
        }
    }

    private void findNextNormalMoveButCollectOverridesAlso() {
        //otherwise has to search for the next move
        for (int y = startY; y < map.getHeight(); y++) {
            for (int x = startX; x < map.getWidth(); x++) {
                if (canPlaceOn(x, y)) {
                    int[] recolorCounts =
                            RecolorCounter.getForAllDirections(player, map, x, y);
                    if ((map.isFree(x, y) || map.isAnyPlayerAt(x, y)) && !Recolorer.hasStonesToRecolor(recolorCounts)) {
                        //if not expansion rule it has to have something to recolor!
                        continue;
                    }
                    int preRating = StaticInfos.getTilePreRating(x, y);
                    if (map.isOverrideableAt(x, y)) {
                        //if would use an override stone
                        Move move = MoveFactory.getRegularMoveFrom(
                                map, player, x, y, 0, recolorCounts);
                        move.setRating(preRating);
                        overrideMoves.add(move);
                        continue;
                    } else {
                        int index = 0;//default for no special Tile
                        if (map.isBonusAt(x, y)) {
                            index = StaticInfo.bonusIndex;
                        } else if (map.isChoiceAt(x, y)) {
                            index = StaticInfo.playerSwapIndex;
                        }
                        for (int specialIndex = 0; specialIndex < StaticInfo.validSpecials[index].length; specialIndex++) {
                            Move move = MoveFactory.getRegularMoveFrom(
                                    map, player, x, y, StaticInfo.validSpecials[index][specialIndex], recolorCounts);
                            move.setRating(preRating);
                            //add prerating for priorityqueue reordering
                            moves.add(move);
                        }
                        hadNormalMoves = true;//
                        startX = x + 1;//update starting point for next iteration
                        startY = y + 1;
                        // return true; //that if statemnent in the beginning is necessary again
                    }
                }
            }
        }
        //return false;
    }
}
