package movegenerator;

import java.util.LinkedList;
import java.util.Queue;

import factories.MoveFactory;

import map.Map;
import move.Move;

public class BombMoveGenerator implements MoveGenerator{
	Queue<Move> moves;

	public BombMoveGenerator(Map map, int player) {
	    moves = new LinkedList<>();
	    collectBombMoves(map,player);
	}
	 
	@Override
    public boolean hasNext() {
        return !moves.isEmpty();
    }

    @Override
    public Move next() {
        return moves.remove();
    }
    
    public int getLength() {
    	return this.moves.size();
    }
    private void collectBombMoves(Map m,int player) {
    	 for (int y = 0; y < m.getHeight(); y++) {
             for (int x = 0; x < m.getWidth(); x++) {

                 if (m.getChar(x, y)!='-')
                	 moves.add(MoveFactory.getBombMoveFrom(m, player, x, y));
             }
    	 }
    }
    
}
