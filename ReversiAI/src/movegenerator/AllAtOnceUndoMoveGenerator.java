package movegenerator;

import factories.MapFactory;
import move.Move;
import move.helper.RecolorCounter;
import move.helper.Recolorer;
import factories.MoveFactory;
import map.Map;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import move.helper.TilePosition;

public class AllAtOnceUndoMoveGenerator implements MoveGenerator {

    public static int[][] validSpecials = {{0}, {Map.BONUS_BOMB, Map.BONUS_OVERRIDE}, {0}};
    public static final int noBonusIndex = 0;
    public static final int bonusIndex = 1;
    public static final int playerSwapIndex = 2;
    Queue<Move> moves;
    private Map map;
    private int player;

    public AllAtOnceUndoMoveGenerator(Map map, int player) {
        this.map = map;
        this.player = player;
        moves = new LinkedList<>();
        updatePlayerSwapChoices();
        collectAllMoves();
    }

    @Override
    public boolean hasNext() {
        return !moves.isEmpty();
    }

    @Override
    public Move next() {
        return moves.remove();
    }

    public int getLength() {
    	return moves.size();
    }
    private final void collectAllMoves() {
        for (int y = 0; y < map.getHeight(); y++) {
            for (int x = 0; x < map.getWidth(); x++) {

                if (canPlaceOn(x, y)) {

                    List<TilePosition> recolorPositions =
                            RecolorCounter.collectAllRecolorPositions(player, map, x, y);
                    if ((map.isFree(x, y) || map.isAnyPlayerAt(x, y)) && recolorPositions.isEmpty()) {
                        //if not expansion rule it has to have something to recolor! 
                        continue;
                    }
                    int index = 0;//default for no specil Tile
                    if (map.isBonusAt(x, y)) {
                        index = bonusIndex;
                    } else if (map.isChoiceAt(x, y)) {
                        index = playerSwapIndex;
                    }

                    for (int i = 0; i < validSpecials[index].length; i++) {
                        moves.add(MoveFactory.getRegularMoveFrom(
                                map, player, x, y, validSpecials[index][i], recolorPositions));
                    }
                }
            }
        }
    }

    private void updatePlayerSwapChoices() {
        int[] players = new int[map.getPlayerCount()];
        for (int i = 0; i < players.length; i++) {
            players[i] = i + 1;
        }
        validSpecials[playerSwapIndex] = players;
    }

    /**
     * A player can place a stone on a tile if it is Free or used but player has
     * an override stone Caution: doesn't check wether any other tiles would be
     * recolored which is neccessary for placing a stone on a free tile or on
     * another player
     */
    private boolean canPlaceOn(int x, int y) {
        //check for expansion rule aswell
        return map.isFree(x, y)
                || ((map.isAnyPlayerAt(x, y) || map.isExpansionAt(x, y))
                && map.getOverrideCountOf(player) > 0);
    }

    public static void main(String[] args) {
        Map map = MapFactory.createFromFile("./maps/phase1/minimalTest.map");
        int player = 1;
        System.out.println(map);
        MoveGenerator gen = new AllAtOnceUndoMoveGenerator(map, player);
        while (gen.hasNext()) {
            System.out.println(gen.next());
        }
    }
}
