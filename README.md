# Reversi_AI

A Java game playing bot for a modified Reversi game used in the 2013 RWTH Aachen i2 Software Lab. It explores the player-move-space using the following search strategies: Greedy, MinMax, Alpha-Beta, Iterative Deepening and Aspiration Windows.

## Usage
All the source code is located in
"ReveriAI/src/". 

All documents, e.g. slides and reports, arein
"Documents/"


Build file

The default build file "build.xml"
is stored in the "ReversiAI/" folder.
It doesn't run test anymore, so it should compile. 
Its default target compiles the source code to 
"ReveriAI/build" and 
creates automatically a ReversiAI.jar file in
"ReversiAI/". 
This jar file can be executed without arguments. 
It will then try to connect to a local server 
127.0.0.1 with port 7777.
Use -h to get help on commandline commands.
